package pubsub

import (
	"context"
	"fmt"

	"github.com/go-logr/logr"
	"github.com/spf13/pflag"
	"github.com/streadway/amqp"
)

type Flags struct {
	brokerURI *string
}

func (f Flags) Connection(ctx context.Context, logger logr.Logger) (*amqp.Connection, error) {
	logger.Info("connecting to amqp")
	conn, err := amqp.Dial(*f.brokerURI)
	if err != nil {
		return nil, fmt.Errorf("failed to dial: %w", err)
	}

	return conn, nil
}

func (f Flags) MustConnection(ctx context.Context, logger logr.Logger) *amqp.Connection {
	conn, err := f.Connection(ctx, logger)
	if err != nil {
		panic(err)
	}

	return conn
}

func (f Flags) Channel(ctx context.Context, logger logr.Logger) (*amqp.Channel, error, func() error) {
	conn, err := f.Connection(ctx, logger)
	if err != nil {
		return nil, fmt.Errorf("failed to create connection: %w", err), nil
	}

	logger.Info("creating amqp channel")
	ch, err := conn.Channel()
	if err != nil {
		return nil, fmt.Errorf("failed to create a channel: %w", err), nil
	}

	// Ensure all our dependencies are set up
	logger.Info("ensuring dependencies")
	err = EnsureDependencies(ctx, ch)
	if err != nil {
		return nil, fmt.Errorf("failed to ensure dependencies: %w", err), nil
	}

	return ch, nil, func() error {
		ch.Close()
		conn.Close()
		return nil
	}
}

func (f Flags) MustChannel(ctx context.Context, logger logr.Logger) (*amqp.Channel, func() error) {
	ch, err, closeCh := f.Channel(ctx, logger)
	if err != nil {
		panic(err)
	}

	return ch, closeCh
}

// Setup flag bindings for flagset to allow us to easily create a
// connection or a channel to our AMQP server.
func SetupFlags(flags *pflag.FlagSet) *Flags {
	return &Flags{
		brokerURI: flags.String(
			"broker-uri",
			"amqp://guest:guest@localhost:5672/",
			"Broker URI (preferably RabbitMQ)",
		),
	}
}
