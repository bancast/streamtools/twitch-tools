package listen

import (
	"context"
	"fmt"

	"github.com/nicklaw5/helix"
	"github.com/spf13/cobra"

	"gitlab.com/bancast/streamtools/twitch-tools/pkg/database"
	dbUsers "gitlab.com/bancast/streamtools/twitch-tools/pkg/database/users"
	"gitlab.com/bancast/streamtools/twitch-tools/pkg/grpc"
	"gitlab.com/bancast/streamtools/twitch-tools/pkg/logging"
	grpcTokens "gitlab.com/bancast/streamtools/twitch-tools/pkg/tokens/grpc"
	"gitlab.com/bancast/streamtools/twitch-tools/pkg/twitch"
	grpcUsers "gitlab.com/bancast/streamtools/twitch-tools/pkg/users/grpc"
)

func NewListenCommand(logger *logging.Logger) *cobra.Command {
	cmd := &cobra.Command{
		Use:   "listen",
		Short: "Listen for new messages",
	}

	dbFlags := database.SetupFlags(cmd.PersistentFlags())

	port := cmd.PersistentFlags().StringP("port", "p", "0.0.0.0:38080", "port to listen on")
	clientID := cmd.PersistentFlags().String("client-id", "", "Twitch OAuth2 Client ID")
	clientSecret := cmd.PersistentFlags().String("client-secret", "", "Twitch OAuth2 Client Secret")
	externalURI := cmd.PersistentFlags().String("external-uri", "", "URL to redirect to after twitch authorisation")
	redirectURI := cmd.PersistentFlags().String("redirect-uri", "", "URL to redirect to after successful authorisation")

	identityClientFlags := grpc.SetupClientFlags("identity", cmd.PersistentFlags())

	cmd.RunE = func(cmd *cobra.Command, args []string) error {
		logger := logger.Logr()
		if *clientID == "" {
			return fmt.Errorf("missing twitch --client-id")
		}
		if *clientSecret == "" {
			return fmt.Errorf("missing twitch --client-secret")
		}

		ctx, cancel := context.WithCancel(cmd.Context())
		defer cancel()

		logger.V(3).Info("creating helix options")
		helixOptions := &helix.Options{
			ClientID:     *clientID,
			ClientSecret: *clientSecret,
			RedirectURI:  *externalURI,
		}

		db := dbFlags.MustDatabase(ctx, logger)
		defer db.Close()

		logger.V(3).Info("creating store adaptors")
		authStore := database.NewAuthStore(ctx, db)
		userStore, _ := dbUsers.NewUserStore(ctx, db)

		tokenGrpcClient := identityClientFlags.MustClient(ctx, logger)
		tokenClient := grpcTokens.NewClient(tokenGrpcClient)
		usersClient := grpcUsers.NewClient(tokenGrpcClient)

		logger.Info("creating twitch controller")
		controller, err := twitch.NewController(helixOptions, authStore, userStore)
		if err != nil {
			return fmt.Errorf("failed to create twitch controller: %w", err)
		}
		logger.Info("created twitch controller")

		logger.Info("starting server")
		return RunServer(
			ctx, logger.WithName("server"),
			*port, *redirectURI,
			controller,
			usersClient,
			tokenClient,
		)
	}

	return cmd
}
