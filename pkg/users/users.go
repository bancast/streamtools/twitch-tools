package users

import (
	"context"
	"time"
)

type User interface {
	GetID() string

	GetCreatedAt() time.Time
	GetUpdatedAt() time.Time

	GetTwitchID() string
	GetBot() bool
	// Deprecated: twitch IDs will be used with tokens to lookup
	// as needed
	GetUsername() string
}

type Store interface {
	List(context.Context) ([]User, error)
	Create(context.Context, User) (User, error)
	Read(context.Context, string) (User, error)
	Lookup(context.Context, string) (User, error)
	Update(context.Context, User) error
	Delete(context.Context, string) error
}

// Create a mapping so we don't need to be close to the database if we
// implement local-side operations
type Controller interface {
	Store
}
