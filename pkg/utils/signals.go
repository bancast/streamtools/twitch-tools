package utils

import (
	"context"
)

func HandleSignals(ctx context.Context) (context.Context, func()) {
	// ctx, stop := signal.NotifyContext(context.Background(), os.Interrupt)
	ctx, stop := context.WithCancel(ctx)
	return ctx, stop
}
