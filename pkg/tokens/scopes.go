package tokens

import (
	"fmt"

	"github.com/hashicorp/go-multierror"
)

type Scope string

const (
	// User scopes
	ScopeUserEmail       Scope = "user_email"
	ScopeUserRead        Scope = "user_read"
	ScopeUserImpersonate Scope = "user_impersonate"
	// Token scopes
	ScopeTokenReadSelf Scope = "token_read_self"
	ScopeTokenRefresh  Scope = "token_refresh"
	ScopeTokenCreate   Scope = "token_create"
	ScopeTokenList     Scope = "token_list"
	// Chat scopes
	ScopeChatRead  Scope = "chat_read"
	ScopeChatWrite Scope = "chat_write"
	// Responses scopes
	ScopeResponsesCreate Scope = "responses_create"
	ScopeResponsesRead   Scope = "responses_read"
	ScopeResponsesUpdate Scope = "responses_update"
	ScopeResponsesDelete Scope = "responses_delete"
	ScopeResponsesList   Scope = "responses_list"
	// Webhooks scopes
	ScopeWebhooksCreate Scope = "webhooks_create"
	ScopeWebhooksRead   Scope = "webhooks_read"
	ScopeWebhooksUpdate Scope = "webhooks_update"
	ScopeWebhooksDelete Scope = "webhooks_delete"
	ScopeWebhooksList   Scope = "webhooks_list"
)

var (
	ScopesDashboard []Scope = []Scope{
		ScopeUserEmail,
		ScopeUserRead,
		ScopeTokenReadSelf,
		ScopeTokenRefresh,
		ScopeTokenCreate,
		ScopeTokenList,
		ScopeChatRead,
		ScopeResponsesCreate,
		ScopeResponsesRead,
		ScopeResponsesUpdate,
		ScopeResponsesDelete,
		ScopeResponsesList,
		ScopeWebhooksCreate,
		ScopeWebhooksRead,
		ScopeWebhooksUpdate,
		ScopeWebhooksDelete,
		ScopeWebhooksList,
	}
)

func ValidateScopes(token Token, scopes []Scope) error {
	var err error
	tokenScopes := ScopesMap(token)

	for _, scope := range scopes {
		if _, ok := tokenScopes[scope]; !ok {
			err = multierror.Append(
				err,
				fmt.Errorf("scope missing: %s", scope),
			)
		}
	}

	if err != nil {
		return fmt.Errorf("token is valid but missing scopes: %w", err)
	}

	return nil
}

func ScopesMap(token Token) map[Scope]struct{} {
	scopes := map[Scope]struct{}{}

	for _, scope := range token.GetScopes() {
		scopes[Scope(scope)] = struct{}{}
	}

	return scopes
}

func StringScopes(scopes []Scope) []string {
	stringScopes := make([]string, 0, len(scopes))

	for _, s := range scopes {
		stringScopes = append(stringScopes, string(s))
	}

	return stringScopes
}
