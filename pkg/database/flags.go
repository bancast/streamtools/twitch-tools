package database

import (
	"context"
	"fmt"

	"github.com/go-logr/logr"
	"github.com/jmoiron/sqlx"
	"github.com/spf13/pflag"
)

type Flags struct {
	databaseURI *string
}

func (f Flags) Database(ctx context.Context, logger logr.Logger) (*sqlx.DB, error) {
	logger.Info("connecting to database")
	db, err := Open(*f.databaseURI)
	if err != nil {
		return nil, fmt.Errorf("failed to open database: %w", err)
	}

	return db, nil
}

func (f Flags) MustDatabase(ctx context.Context, logger logr.Logger) *sqlx.DB {
	db, err := f.Database(ctx, logger)
	if err != nil {
		panic(err)
	}

	return db
}

// databaseFlags.Setup()
func SetupFlags(flags *pflag.FlagSet) *Flags {
	return &Flags{
		databaseURI: flags.String(
			"database-uri",
			"postgres://postgres:password@localhost:5432/twitch_tools?sslmode=disable",
			"Database URI (preferably postgres)",
		),
	}
}
