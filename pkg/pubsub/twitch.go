package pubsub

import (
	"context"
	"fmt"

	"log"

	"github.com/streadway/amqp"
	"google.golang.org/protobuf/proto"

	"gitlab.com/bancast/streamtools/twitch-tools/protos/twitch/messages"
)

const (
	ProtobufContentType       string = "application/vnd.google.protobuf"
	TwitchMessageExchangeName string = "twitch.messages"
)

var (
	TwitchMessageQueues []string = []string{
		"log",
		"webhook",
		"responder",
	}
)

func EnsureDependencies(ctx context.Context, ch *amqp.Channel) error {
	err := ch.ExchangeDeclare(
		TwitchMessageExchangeName, // name
		"fanout",                  // type
		true,                      // durable
		false,                     // auto-deleted
		false,                     // internal
		false,                     // no-wait
		nil,                       // arguments
	)
	if err != nil {
		return fmt.Errorf("failed to declare exchange %s: %w", TwitchMessageExchangeName, err)
	}

	for _, name := range TwitchMessageQueues {
		queueName := twitchQueueName(name)
		msgQueue, err := ch.QueueDeclare(
			queueName, // name
			true,      // durable
			false,     // delete when unused
			false,     // exclusive
			false,     // no-wait
			nil,       // arguments
		)
		if err != nil {
			return fmt.Errorf("failed to declare queue %s: %w", queueName, err)
		}

		err = ch.QueueBind(
			msgQueue.Name,             // queue name
			"",                        // routing key
			TwitchMessageExchangeName, // exchange
			false,
			nil,
		)
		if err != nil {
			return fmt.Errorf("failed to bind queue %s to exchange %s: %w", msgQueue.Name, TwitchMessageExchangeName, err)
		}
	}

	return nil
}

func PublishMessage(ctx context.Context, ch *amqp.Channel, msg *messages.TwitchMessage) error {
	body, err := proto.Marshal(msg)
	if err != nil {
		return fmt.Errorf("failed to marshal message: %w", err)
	}
	ch.Publish(
		TwitchMessageExchangeName, // exchange
		"",                        // routing key
		false,                     // mandatory
		false,                     // immediate
		amqp.Publishing{
			ContentType: ProtobufContentType,
			Body:        body,
		},
	)

	return nil
}

// NewTemporaryQueue creates a temporary queue on RabbitMQ that is
// bound to the exchange but is removed when the bot is not connected.
func NewTemporaryQueue(ctx context.Context, ch *amqp.Channel) (string, error) {
	msgQueue, err := ch.QueueDeclare(
		"",    // generate our name
		true,  // durable
		true,  // delete when unused
		true,  // exclusive
		false, // no-wait
		nil,   // arguments
	)
	if err != nil {
		return "", fmt.Errorf("failed to declare temporary queue: %w", err)
	}

	err = ch.QueueBind(
		msgQueue.Name,             // queue name
		"",                        // routing key
		TwitchMessageExchangeName, // exchange
		false,
		nil,
	)
	if err != nil {
		return "", fmt.Errorf("failed to bind queue %s to exchange %s: %w", msgQueue.Name, TwitchMessageExchangeName, err)
	}

	return msgQueue.Name, nil
}

// ReadTwitchMessages provides a helper around ReadMessages to use a
// simpler name for previously defined queues.
func ReadTwitchMessages(ctx context.Context, ch *amqp.Channel, name string, msgChan chan<- *messages.TwitchMessage) error {
	return ReadMessages(ctx, ch, twitchQueueName(name), msgChan)
}

// ReadMessages will start consuming messages from a queue in
// RabbitMQ. These will then be published to the go channel to be
// processed.
func ReadMessages(ctx context.Context, ch *amqp.Channel, name string, msgChan chan<- *messages.TwitchMessage) error {
	msgs, err := ch.Consume(
		name,  // queue
		"",    // consumer
		true,  // auto-ack
		false, // exclusive
		false, // no-local
		false, // no-wait
		nil,   // args
	)
	if err != nil {
		return fmt.Errorf("failed to register consumer: %w", err)
	}

	for msg := range msgs {
		if msg.ContentType != ProtobufContentType {
			log.Printf("wrong content type on queue detected %s; skipping this message", msg.ContentType)
			continue
		}

		var message messages.TwitchMessage
		err = proto.Unmarshal(msg.Body, &message)
		msgChan <- &message
	}

	return nil
}

func twitchQueueName(name string) string {
	return fmt.Sprintf("%s.%s", TwitchMessageExchangeName, name)
}
