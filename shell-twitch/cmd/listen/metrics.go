package listen

import "go.opencensus.io/stats/view"

var DefaultViews = []*view.View{
	MessageLatencyView,
	MessageTotalView,
}

func RegisterDefaultViews() {
	view.Register(DefaultViews...)
}
