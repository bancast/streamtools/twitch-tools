package listen

import (
	"context"
	"fmt"
	"net/http"
	"net/url"
	"time"

	"github.com/go-logr/logr"
	"github.com/nicklaw5/helix"

	"gitlab.com/bancast/streamtools/twitch-tools/pkg/auth"
	dbUsers "gitlab.com/bancast/streamtools/twitch-tools/pkg/database/users"
	"gitlab.com/bancast/streamtools/twitch-tools/pkg/tokens"
	"gitlab.com/bancast/streamtools/twitch-tools/pkg/twitch"
	"gitlab.com/bancast/streamtools/twitch-tools/pkg/users"
)

type LoginEvent struct {
	Username string
}

const (
	DefaultDashboardTokenLifetime time.Duration = 24 * time.Hour
)

var (
	TwitchTokenScopes []string = []string{
		"channel:moderate",
		"chat:edit",
		"chat:read",
		"user:read:email",
	}
)

// This server is used to provide an OAuth token to the server for the user
func RunServer(ctx context.Context, logger logr.Logger, location string, redirectURI string, controller *twitch.Controller, userController users.Controller, tokenController tokens.Controller) error {
	logger = logger.WithName("http")
	redirectURL, err := url.Parse(redirectURI)
	if err != nil {
		return fmt.Errorf("invalid uri format for redirect: %w", err)
	}

	http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		nonce := r.URL.Query().Get("nonce")
		if nonce == "" {
			fmt.Fprintln(w, "failed to get nonce from query")
			w.WriteHeader(http.StatusBadRequest)
			return
		}

		logger.Info("login request")
		url := controller.HelixClient.GetAuthorizationURL(&helix.AuthorizationURLParams{
			ResponseType: "code",
			Scopes:       TwitchTokenScopes,
			State:        nonce,
		})
		logger.V(3).Info("forwarding to authorisation server", "url", url)
		http.Redirect(w, r, url, http.StatusTemporaryRedirect)
	})

	http.HandleFunc("/authenticate", func(w http.ResponseWriter, r *http.Request) {
		ctx := r.Context()
		logger.Info("received authentication request")

		code := r.URL.Query().Get("code")
		if code == "" {
			logger.Info("empty code in url query")
			fmt.Fprintln(w, "empty code in url query")
			w.WriteHeader(http.StatusBadRequest)
			return
		}

		// Get the nonce. We need this to be able to create our tokens, otherwise any response could get an id
		state := r.URL.Query().Get("state")
		if state == "" {
			logger.Info("empty state in url query")
			fmt.Fprintln(w, "empty state in url query")
			w.WriteHeader(http.StatusBadRequest)
			return
		}
		nonce := state

		logger.Info("requesting access token using code")
		resp, err := controller.HelixClient.RequestUserAccessToken(code)
		if err != nil {
			logger.Error(err, "failed to exchange code for access token")
			fmt.Fprintln(w, "failed to exchange code for access token")
			w.WriteHeader(http.StatusInternalServerError)
			return
		}
		logger.V(3).Info("exchanged code for access token")

		logger.V(3).Info("creating helix client")
		client, err := controller.Client(
			ctx,
			&auth.Tokens{
				AccessToken:  resp.Data.AccessToken,
				RefreshToken: resp.Data.RefreshToken,
			},
		)
		if err != nil {
			logger.Error(err, "failed to create client")
			return
		}

		logger.Info("looking up owner of token")
		user, err := client.Self(ctx)
		if err != nil {
			logger.Error(err, "failed to find user for the token")
			return
		}

		logger = logger.WithValues(
			"twitch_user_id", user.ID,
			"twitch_user_login", user.Login,
		)

		// Create does a get or create under the hood
		//
		// TODO: Add support for logging in bots
		dbUser, err := userController.Create(ctx, dbUsers.NewUser("N/A", &user.ID, user.Login, false))
		if err != nil {
			logger.Error(err, "failed to get or create user")
			return
		}

		logger.Info("storing new token")
		err = client.StoreToken(ctx, user.ID)
		if err != nil {
			logger.Error(err, "failed to store token")
			return
		}

		// Create client token
		tokenResp, err := tokenController.Create(ctx, &tokens.CreateOptions{
			Nonce:    nonce,
			User:     dbUser.GetID(),
			Lifetime: DefaultDashboardTokenLifetime,
			Name:     "dashboard",
			Comment:  "Created for dashboard use",
			Scopes:   tokens.StringScopes(tokens.ScopesDashboard),
		})

		redirectURI := *redirectURL
		redirectURI.Fragment = tokenResp
		logger.Info("authentication event registered")
		logger.Info("redirecting user to dashboard", "url", redirectURI.String())
		http.Redirect(w, r, redirectURI.String(), http.StatusTemporaryRedirect)
	})

	logger.Info("http server listening", "location", location)
	return http.ListenAndServe(location, nil)
}
