package webhooks

import "context"

type Store interface {
	List(context.Context, string) ([]Webhook, error)
	Create(context.Context, string, Webhook) (string, error)
	Read(context.Context, string) (Webhook, error)
	Update(context.Context, Webhook) error
	Delete(context.Context, string) error
}
