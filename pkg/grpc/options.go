package grpc

import "fmt"

type Options struct {
	AllowedMethods []string
}

type Option func(options *Options) error

func WithAllowedMethods(allowedMethods []string) Option {
	return func(options *Options) error {
		options.AllowedMethods = allowedMethods
		return nil
	}
}

// WithOptions allows the setting of options for the grpc server
func (f *ServerFlags) WithOptions(options ...Option) error {
	for _, opt := range options {
		err := opt(&f.options)
		if err != nil {
			return fmt.Errorf("failed to set option: %w", err)
		}
	}

	return nil
}
