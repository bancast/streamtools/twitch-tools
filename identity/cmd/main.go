package cmd

import (
	"fmt"
	"os"

	"github.com/spf13/cobra"
	"gitlab.com/bancast/streamtools/twitch-tools/identity/cmd/listen"
	"gitlab.com/bancast/streamtools/twitch-tools/identity/cmd/tokens"
	"gitlab.com/bancast/streamtools/twitch-tools/pkg/logging"
)

func NewRootCmd() *cobra.Command {
	cmd := &cobra.Command{
		Use:   "identity",
		Short: "The frontend for the identity components of twitch-tools",
	}

	logger := logging.SetupLoggerFlags(cmd.PersistentFlags())

	cmd.AddCommand(listen.NewListenCommand(logger))
	cmd.AddCommand(tokens.NewTokensCommand(logger))

	return cmd
}

func Execute() {
	rootCmd := NewRootCmd()

	if err := rootCmd.Execute(); err != nil {
		fmt.Fprintln(os.Stderr, err)
		os.Exit(1)
	}
}
