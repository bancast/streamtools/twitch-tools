package responses

import (
	"context"
	"fmt"
	"time"
)

var _ Store = &CachedStore{}

func NewCachedStore(i Store, ttl time.Duration) *CachedStore {
	return &CachedStore{
		Store: i,
		TTL:   ttl,
		Cache: make(map[string]CachedResponses),
	}
}

type CachedResponses struct {
	Time      time.Time
	Responses []Response
}

type CachedStore struct {
	Store
	TTL   time.Duration
	Cache map[string]CachedResponses
}

func (getter *CachedStore) List(ctx context.Context, twitch_id string) ([]Response, error) {

	if cacheEntry, ok := getter.Cache[twitch_id]; ok && time.Since(cacheEntry.Time) < getter.TTL {
		return cacheEntry.Responses, nil
	}

	responses, err := getter.Store.List(ctx, twitch_id)
	if err != nil {
		return nil, fmt.Errorf("failed to get responses: %w", err)
	}

	getter.Cache[twitch_id] = CachedResponses{
		Time:      time.Now(),
		Responses: responses,
	}

	return responses, nil
}
