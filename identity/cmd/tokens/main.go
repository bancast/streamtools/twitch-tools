package tokens

import (
	"crypto/rand"
	"encoding/base64"
	"fmt"

	"github.com/spf13/cobra"

	grpcUtils "gitlab.com/bancast/streamtools/twitch-tools/pkg/grpc"
	"gitlab.com/bancast/streamtools/twitch-tools/pkg/logging"
)

func NewTokensCommand(logger *logging.Logger) *cobra.Command {
	cmd := &cobra.Command{
		Use:   "tokens",
		Short: "Manage token concerns",
	}

	tokenClientFlags := grpcUtils.SetupClientFlags("identity", cmd.PersistentFlags())
	username := cmd.PersistentFlags().String("username", "jackatbancast", "user to create token for")

	cmd.AddCommand(NewCreateCommand(logger, tokenClientFlags, username))

	return cmd
}

func makeNonce() (string, error) {
	nonce := make([]byte, 64)

	_, err := rand.Read(nonce)
	if err != nil {
		return "", fmt.Errorf("failed to get a nonce")
	}

	return base64.StdEncoding.EncodeToString(nonce), nil
}
