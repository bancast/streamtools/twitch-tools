package jsonnetenv

import (
	"context"
	"encoding/json"
	"fmt"
	"path/filepath"

	jsonnet "github.com/google/go-jsonnet"
)

type VMOption func(context.Context, *VM) error

func WithFileImporter(basepath string, jpaths []string) VMOption {
	basepath, err := filepath.Abs(basepath)
	basedir := filepath.Dir(basepath)
	if err != nil {
		return nil
	}

	return withJPaths(append([]string{basedir}, jpaths...))
}

func withJPaths(jpaths []string) VMOption {
	return func(_ context.Context, vm *VM) error {
		vm.VM.Importer(&jsonnet.FileImporter{
			JPaths: jpaths,
		})
		return nil
	}
}

func WithStringOutput(output bool) VMOption {
	return func(_ context.Context, vm *VM) error {
		vm.VM.StringOutput = output
		return nil
	}
}

type Var struct {
	Key   string
	Value string
}

func WithExtVars(vars []Var) VMOption {
	return func(_ context.Context, vm *VM) error {
		for _, extVar := range vars {
			vm.VM.ExtVar(extVar.Key, extVar.Value)
		}

		return nil
	}
}

func WithTLAVars(vars []Var) VMOption {
	return func(c_ context.Context, vm *VM) error {
		for _, tla := range vars {
			vm.VM.TLAVar(tla.Key, tla.Value)
		}

		return nil
	}
}

type Code struct {
	Key   string
	Value interface{}
}

func WithExtCodes(codes []Code) VMOption {
	return func(_ context.Context, vm *VM) error {
		for _, extCode := range codes {
			value, err := json.Marshal(extCode.Value)
			if err != nil {
				return fmt.Errorf("could not marshal %v into json", extCode.Value)
			}
			vm.VM.ExtCode(extCode.Key, string(value))
		}

		return nil
	}
}

func WithTLACodes(codes []Code) VMOption {
	return func(c_ context.Context, vm *VM) error {
		for _, tla := range codes {
			value, err := json.Marshal(tla.Value)
			if err != nil {
				return fmt.Errorf("could not marshal %v into json", tla.Value)
			}
			vm.VM.TLACode(tla.Key, string(value))
		}

		return nil
	}
}
