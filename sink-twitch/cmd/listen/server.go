package listen

import (
	"context"

	"github.com/go-logr/logr"
	"github.com/streadway/amqp"
	grpcUtils "gitlab.com/bancast/streamtools/twitch-tools/pkg/grpc"
	"gitlab.com/bancast/streamtools/twitch-tools/pkg/pubsub"
	"gitlab.com/bancast/streamtools/twitch-tools/protos/twitch/messages"
	"gitlab.com/bancast/streamtools/twitch-tools/protos/twitch/sink"
	"google.golang.org/grpc"
	"google.golang.org/grpc/codes"
)

type TwitchSinkServer struct {
	sink.UnimplementedTwitchMessageSinkServer
	grpcUtils.DisabledAuth
	channel *amqp.Channel
	logger  logr.Logger
}

func (t TwitchSinkServer) Sink(ctx context.Context, msg *messages.TwitchMessage) (*sink.TwitchMessageSinkResponse, error) {
	logger := t.logger
	logger.Info("receiving message")
	message := msg.Msg
	if message == nil {
		err := grpc.Errorf(codes.FailedPrecondition, "no message body sent")
		logger.Error(err, "no message body")
		return nil, err
	}
	// TODO(jackatbancast): Add more validation
	logger.Info("publishing message")
	err := pubsub.PublishMessage(ctx, t.channel, msg)
	if err != nil {
		logger.Error(err, "failed to publish")
		return nil, grpc.Errorf(codes.Internal, "failed to publish message")
	}

	logger.Info("published message")
	return &sink.TwitchMessageSinkResponse{Created: true}, nil
}
