package jsonnetenv

import (
	"fmt"
	"os"

	"github.com/go-git/go-git/v5"
	"github.com/go-git/go-git/v5/plumbing"
)

func WithGitRevisionNativeFunction() VMOption {
	return WithNativeFunction("gitRevision", []string{"rev"}, func(params []interface{}) (interface{}, error) {
		revString, ok := params[0].(string)
		if !ok {
			return nil, fmt.Errorf("rev is not a string")
		}

		cwd, err := os.Getwd()
		if err != nil {
			return nil, err
		}

		repo, err := git.PlainOpenWithOptions(cwd, &git.PlainOpenOptions{DetectDotGit: true})
		if err != nil {
			return nil, err
		}

		ref, err := repo.ResolveRevision(plumbing.Revision(revString))
		if err != nil {
			return nil, err
		}

		return ref.String(), nil
	})
}
