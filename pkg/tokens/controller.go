package tokens

import (
	"context"
	"fmt"

	"github.com/hashicorp/go-multierror"
)

var _ Controller = &LocalController{}

// Controller implements our core logic for managing token lifecycles
type LocalController struct {
	store       Store
	provisioner Provisioner
}

// Create a NewController with the given store and provisioner
func NewController(ctx context.Context, store Store, provisioner Provisioner) *LocalController {
	return &LocalController{
		store:       store,
		provisioner: provisioner,
	}
}

// Create a token and return it's ID
func (c LocalController) Create(ctx context.Context, options *CreateOptions) (string, error) {
	ctx, cancel := context.WithCancel(ctx)
	defer cancel()

	id, err := c.store.Create(ctx, options)
	if err != nil {
		return "", fmt.Errorf("failed to create token: %w", err)
	}

	return id, nil
}

// List tokens
func (c LocalController) List(ctx context.Context) ([]Token, error) {
	ctx, cancel := context.WithCancel(ctx)
	defer cancel()

	tokens, err := c.store.List(ctx)
	if err != nil {
		return nil, fmt.Errorf("failed to list tokens")
	}

	return tokens, nil
}

// Read token details from the database
func (c LocalController) Read(ctx context.Context, id string) (Token, error) {
	ctx, cancel := context.WithCancel(ctx)
	defer cancel()

	token, err := c.store.Read(ctx, &ReadOptions{id})
	if err != nil {
		return nil, fmt.Errorf("failed to read token: %w", err)
	}

	return token, nil
}

// Exchange an id and nonce for a token
func (c LocalController) Exchange(ctx context.Context, id string, nonce string) (string, error) {
	ctx, cancel := context.WithCancel(ctx)
	defer cancel()

	// Start the token exchange in the store
	transaction, err := c.store.Exchange(ctx, &ExchangeOptions{
		ID:    id,
		Nonce: nonce,
	})
	if err != nil {
		return "", fmt.Errorf("failed to create request: %w", err)
	}

	// Get a handle on our options
	options := transaction.Options()

	// Actually Create the token
	token, err := c.provisioner.New(ctx, options)
	if err != nil {
		tErr := transaction.Error()
		if tErr != nil {
			err = multierror.Append(err, tErr)
		}
		// Tell the exchange that we failed to create the token
		return "", err
	}

	// Try to tell the exchange we created the token fine
	if err := transaction.Complete(); err != nil {
		// Don't return a token if we can't complete the exchange
		return "", fmt.Errorf("failed to complete token exchange: %w", err)
	}

	return token, nil
}

// Refresh a token if it has the required permissions
func (c LocalController) Refresh(ctx context.Context) (string, error) {
	ctx, cancel := context.WithCancel(ctx)
	defer cancel()

	ctxProvisioner := ContextProvisioner{Provisioner: c.provisioner}
	token, err := ctxProvisioner.GetToken(ctx)
	if err != nil {
		return "", fmt.Errorf("failed to get token from context: %w", err)
	}

	err = ValidateScopes(token, []Scope{ScopeTokenRefresh})
	if err != nil {
		return "", fmt.Errorf("token is missing required scopes: %w", err)
	}

	// Start the token refresh in the store
	transaction, err := c.store.Refresh(ctx, &RefreshOptions{
		TokenID: token.GetTokenID(),
		UserID:  token.GetUserID(),
	})
	if err != nil {
		return "", fmt.Errorf("failed to create request")
	}

	// Get a handle on our options
	options := transaction.Options()

	// Actually Create the token
	tokenString, err := c.provisioner.New(ctx, options)
	if err != nil {
		tErr := transaction.Error()
		if tErr != nil {
			err = multierror.Append(err, tErr)
		}
		// Tell the refresh that we failed to create the token
		return "", err
	}

	// Try to tell the exchange we created the token fine
	if err := transaction.Complete(); err != nil {
		// Don't return a token if we can't complete the exchange
		return "", fmt.Errorf("failed to complete token exchange: %w", err)
	}

	return tokenString, nil
}

// Validate a given token has all of the scopes required
func (c LocalController) Validate(ctx context.Context, tokenString string, scopes []Scope) (Token, error) {
	token, err := c.provisioner.Validate(ctx, tokenString, scopes)
	if err != nil {
		return nil, fmt.Errorf("token is not valid: %w", err)
	}

	return token, nil
}
