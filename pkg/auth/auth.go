package auth

import (
	"context"

	"gitlab.com/bancast/streamtools/twitch-tools/pkg/users"
)

type Tokens struct {
	AccessToken  string
	RefreshToken string
}

type TokenStore interface {
	Get(context.Context, string) (*Tokens, error)
	Store(context.Context, string, Tokens) error
}

type UserStore interface {
	List(context.Context) ([]users.User, error)
	Read(context.Context, string) (users.User, error)
	Update(context.Context, users.User) error
}
