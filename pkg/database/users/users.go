package database

import (
	"context"
	"fmt"
	"time"

	"github.com/google/uuid"
	"github.com/jmoiron/sqlx"
	users "gitlab.com/bancast/streamtools/twitch-tools/pkg/users"
)

var _ users.Store = &DatabaseUsersStore{}

const (
	ListQuery string = `
select *
from streaming.users
order by username desc;
`
	CreateQuery string = `
insert into streaming.users
  (twitch_id, username, bot)
VALUES
  ($1, $2, $3)
ON CONFLICT
  (twitch_id)
DO UPDATE SET
  bot=$3
returning *;
`
	UpdateQuery string = `
update streaming.users
set
  username=$2,
  bot=$3
where id = $1;
`
	ReadQuery string = `
select *
from streaming.users
where id = $1;
`
	LookupQuery string = `
select *
from streaming.users
where username = $1;
`
	DeleteQuery string = `
delete
from streaming.users
where id = $1;
`
)

func NewUser(id string, twitch_id *string, username string, bot bool) *User {
	uid, _ := uuid.Parse(id)
	return &User{
		ID:       uid,
		TwitchID: twitch_id,
		Username: username,
		Bot:      bot,
	}
}

type User struct {
	ID        uuid.UUID `db:"id"`
	TwitchID  *string   `db:"twitch_id"`
	CreatedAt time.Time `db:"created_at"`
	UpdatedAt time.Time `db:"updated_at"`
	Username  string    `db:"username"`
	Bot       bool      `db:"bot"`
}

func (u User) GetID() string {
	return u.ID.String()
}

func (u User) GetTwitchID() string {
	if u.TwitchID == nil {
		return ""
	}

	return *u.TwitchID
}

func (u User) GetCreatedAt() time.Time {
	return u.CreatedAt
}

func (u User) GetUpdatedAt() time.Time {
	return u.UpdatedAt
}

func (u User) GetUsername() string {
	return u.Username
}

func (u User) GetBot() bool {
	return u.Bot
}

func NewUserStore(ctx context.Context, db *sqlx.DB) (*DatabaseUsersStore, error) {
	getter := &DatabaseUsersStore{
		DB: db,
	}

	return getter, getter.Health(ctx)
}

type DatabaseUsersStore struct {
	DB *sqlx.DB
}

func (d *DatabaseUsersStore) List(ctx context.Context) ([]users.User, error) {
	stmt, err := d.DB.PreparexContext(ctx, ListQuery)
	if err != nil {
		return nil, fmt.Errorf("failed to prepare statement: %w", err)
	}

	users := []User{}
	err = stmt.SelectContext(ctx, &users)
	if err != nil {
		return nil, fmt.Errorf("Failed to query users: %w", err)
	}

	return mapUsers(users), nil
}

func mapUsers(inUsers []User) []users.User {
	users := make([]users.User, 0, len(inUsers))
	for _, user := range inUsers {
		users = append(users, user)
	}
	return users
}

func (d *DatabaseUsersStore) Create(ctx context.Context, user users.User) (users.User, error) {
	stmt, err := d.DB.PreparexContext(ctx, CreateQuery)
	if err != nil {
		return nil, fmt.Errorf("failed to prepare statement: %w", err)
	}

	var u User
	err = stmt.GetContext(ctx, &u, user.GetTwitchID(), user.GetUsername(), user.GetBot())
	if err != nil {
		return nil, fmt.Errorf("failed to create/get new user: %w", err)
	}

	return u, nil
}

func (d *DatabaseUsersStore) Read(ctx context.Context, id string) (users.User, error) {
	uid, err := uuid.Parse(id)
	if err != nil {
		return nil, fmt.Errorf("failed to parse ID (%s) to get: %w", uid, err)
	}

	stmt, err := d.DB.PreparexContext(ctx, ReadQuery)
	if err != nil {
		return nil, fmt.Errorf("failed to prepare statement: %w", err)
	}

	var user User
	err = stmt.GetContext(ctx, &user, uid)
	if err != nil {
		return nil, fmt.Errorf("failed to get user: %w", err)
	}

	return user, nil
}

func (d *DatabaseUsersStore) Lookup(ctx context.Context, username string) (users.User, error) {
	stmt, err := d.DB.PreparexContext(ctx, LookupQuery)
	if err != nil {
		return nil, fmt.Errorf("failed to prepare statement: %w", err)
	}

	var user User
	err = stmt.GetContext(ctx, &user, username)
	if err != nil {
		return nil, fmt.Errorf("failed to get user: %w", err)
	}

	return user, nil
}

func (d *DatabaseUsersStore) Update(ctx context.Context, user users.User) error {
	uid, err := uuid.Parse(user.GetID())
	if err != nil {
		return fmt.Errorf("failed to parse ID (%s) to get: %w", uid, err)
	}

	stmt, err := d.DB.PreparexContext(ctx, UpdateQuery)
	if err != nil {
		return fmt.Errorf("failed to prepare statement: %w", err)
	}

	result, err := stmt.ExecContext(
		ctx,
		uid,
		user.GetUsername(),
		user.GetBot(),
	)
	if err != nil {
		return fmt.Errorf("failed to get user: %w", err)
	}

	num, err := result.RowsAffected()
	if err != nil {
		return fmt.Errorf("failed to get number of rows affected: %w", err)
	}

	if num != 1 {
		return fmt.Errorf("unexpected number of rows affected: %d", num)
	}

	return nil
}

func (d *DatabaseUsersStore) Delete(ctx context.Context, id string) error {
	uid, err := uuid.Parse(id)
	if err != nil {
		return fmt.Errorf("failed to parse ID (%s) to get: %w", uid, err)
	}

	stmt, err := d.DB.PreparexContext(ctx, DeleteQuery)
	if err != nil {
		return fmt.Errorf("failed to prepare statement: %w", err)
	}

	_, err = stmt.ExecContext(ctx, uid)
	if err != nil {
		return fmt.Errorf("failed to get user: %w", err)
	}

	return nil
}

func (d *DatabaseUsersStore) Health(ctx context.Context) error {
	return d.DB.PingContext(ctx)
}
