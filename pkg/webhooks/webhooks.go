package webhooks

type Webhook interface {
	GetID() string
	GetURL() string
	GetSecret() string
}
