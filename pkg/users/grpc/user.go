package grpc

import (
	"time"

	"gitlab.com/bancast/streamtools/twitch-tools/pkg/users"
)

var _ users.User = &User{}

type User struct {
	Id        string
	CreatedAt time.Time
	UpdatedAt time.Time
	TwitchID  *string
	Username  string
	Bot       bool
}

func (u User) GetID() string {
	return u.Id
}

func (u User) GetTwitchID() string {
	if u.TwitchID == nil {
		return ""
	}

	return *u.TwitchID
}

func (u User) GetCreatedAt() time.Time {
	return u.CreatedAt
}

func (u User) GetUpdatedAt() time.Time {
	return u.UpdatedAt
}

func (u User) GetUsername() string {
	return u.Username
}

func (u User) GetBot() bool {
	return u.Bot
}
