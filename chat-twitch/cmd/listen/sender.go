package listen

import (
	"context"
	"fmt"

	twitchIRC "github.com/gempir/go-twitch-irc/v2"
	"github.com/go-logr/logr"
	grpcUtils "gitlab.com/bancast/streamtools/twitch-tools/pkg/grpc"
	"gitlab.com/bancast/streamtools/twitch-tools/pkg/twitch"
	"gitlab.com/bancast/streamtools/twitch-tools/protos/twitch/chat"
	"google.golang.org/grpc"
	"google.golang.org/grpc/codes"
)

type IRCClient struct {
	*twitchIRC.Client
	Channel string
}

type TwitchChatServer struct {
	chat.UnimplementedTwitchChatServer
	grpcUtils.DisabledAuth
	// Channel -> irc client
	clients    map[string]*IRCClient
	controller *twitch.Controller
	logger     logr.Logger
}

func NewChatServer(logger logr.Logger, controller *twitch.Controller) *TwitchChatServer {
	return &TwitchChatServer{
		controller: controller,
		clients:    map[string]*IRCClient{},
		logger:     logger,
	}
}

func (t TwitchChatServer) Send(ctx context.Context, msg *chat.TwitchChatRequest) (*chat.TwitchChatResponse, error) {
	logger := t.logger.WithValues("twitch_id", msg.TwitchId)
	logger.Info("incoming send request")
	if len(msg.Body) == 0 {
		err := grpc.Errorf(codes.FailedPrecondition, "no message to send")
		logger.Error(err, "no message to send")
		return nil, err
	}

	client, err := t.getOrCreateClient(ctx, logger, msg.TwitchId)
	if err != nil {
		logger.Error(err, "failed to create client")
		return nil, grpc.Errorf(codes.Internal, "failed to create client")
	}

	logger.V(2).Info("saying message", "msg", msg.Body)
	client.Say(client.Channel, msg.Body)

	return &chat.TwitchChatResponse{Sent: true}, nil
}

func (t TwitchChatServer) getOrCreateClient(ctx context.Context, logger logr.Logger, twitch_id string) (*IRCClient, error) {
	// logger := logger.V(2)
	var client *twitchIRC.Client

	if c, ok := t.clients[twitch_id]; ok {
		return c, nil
	}

	logger.Info("creating user client")
	helixClient, err := t.controller.UserClient(ctx, twitch_id)
	if err != nil {
		return nil, fmt.Errorf("failed to create client for %s: %w", twitch_id, err)
	}

	logger.Info("refreshing token")
	err = helixClient.MaybeRefresh(ctx)
	if err != nil {
		return nil, fmt.Errorf("failed to refresh token for %s: %w", twitch_id, err)
	}

	user, err := helixClient.Self(ctx)
	if err != nil {
		return nil, fmt.Errorf("failed to get self: %w", err)
	}

	logger.Info("creating irc client")
	client = twitchIRC.NewClient(user.Login, fmt.Sprintf("oauth:%s", helixClient.Tokens.AccessToken))

	logger.Info("joining channel")
	client.Join(user.Login)

	logger.Info("conntecting to irc")
	go func() {
		err = client.Connect()
		if err != nil {
			logger.Error(err, "failed to connect")
		}
	}()

	logger.Info("caching IRC client")
	c := &IRCClient{Client: client, Channel: user.Login}
	t.clients[twitch_id] = c

	logger.Info("returning irc client")
	return c, nil
}
