package logging

import (
	"fmt"

	"github.com/go-logr/logr"
	"github.com/go-logr/zapr"
	flag "github.com/spf13/pflag"
	"go.uber.org/zap"
)

type Logger struct {
	Verbosity *int
}

func SetupLoggerFlags(flags *flag.FlagSet) *Logger {
	return &Logger{
		Verbosity: flags.IntP("verbosity", "v", 0, "Verbosity of logs"),
	}
}

func (l Logger) Logr() logr.Logger {
	zapLog, err := zap.NewDevelopment()
	if err != nil {
		panic(fmt.Sprintf("unexpected error creating logger: %s", err))
	}
	return zapr.NewLogger(zapLog)
}
