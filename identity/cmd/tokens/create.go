package tokens

import (
	"context"
	"fmt"
	"os"
	"time"

	"github.com/spf13/cobra"

	grpcUtils "gitlab.com/bancast/streamtools/twitch-tools/pkg/grpc"
	"gitlab.com/bancast/streamtools/twitch-tools/pkg/logging"
	"gitlab.com/bancast/streamtools/twitch-tools/pkg/tokens"
	grpcTokens "gitlab.com/bancast/streamtools/twitch-tools/pkg/tokens/grpc"
	grpcUsers "gitlab.com/bancast/streamtools/twitch-tools/pkg/users/grpc"
)

func NewCreateCommand(logger *logging.Logger, tokenClientFlags *grpcUtils.ClientFlags, username *string) *cobra.Command {
	cmd := &cobra.Command{
		Use:   "create",
		Short: "Create a token for a user",
	}

	tokenName := cmd.PersistentFlags().String("token-name", "cli-provisioned-token", "name of the created token")
	tokenLifetime := cmd.PersistentFlags().Duration("token-lifetime", 24*time.Hour, "lifetime of the provisioned token")
	tokenScopes := cmd.PersistentFlags().StringSlice("token-scopes", []string{string(tokens.ScopeTokenRefresh)}, "scopes to bind to the token")
	tokenComment := cmd.PersistentFlags().String(
		"token-comment",
		fmt.Sprintf("token provisioned by twitch-tools identity cli - %s", time.Now().UTC()),
		"Comment to add to the token",
	)

	cmd.RunE = func(cmd *cobra.Command, args []string) error {
		logger := logger.Logr()
		ctx, cancel := context.WithCancel(cmd.Context())
		defer cancel()

		grpcClient := tokenClientFlags.MustClient(ctx, logger)
		defer grpcClient.Close()
		tokenClient := grpcTokens.NewClient(grpcClient)
		userClient := grpcUsers.NewClient(grpcClient)

		logger = logger.WithValues("user", *username)

		// Get user id for token
		logger.V(2).Info("matching user id to token")
		user, err := userClient.Lookup(ctx, *username)
		if err != nil {
			return fmt.Errorf("failed to lookup user %s: %w", *username, err)
		}

		logger = logger.WithValues("user_id", user.GetID())
		logger.V(1).Info("found user")

		logger.V(2).Info("generating nonce")
		nonce, err := makeNonce()
		if err != nil {
			return err
		}
		logger = logger.WithValues("nonce", nonce)
		logger.V(2).Info("generated nonce")

		logger.V(2).Info("generating token authorisation")
		tokenId, err := tokenClient.Create(ctx, &tokens.CreateOptions{
			User:     user.GetID(),
			Nonce:    nonce,
			Name:     *tokenName,
			Lifetime: *tokenLifetime,
			Scopes:   *tokenScopes,
			Comment:  *tokenComment,
		})
		if err != nil {
			return fmt.Errorf("failed to create token authorisation: %w", err)
		}
		logger = logger.WithValues("token_id", tokenId)
		logger.V(1).Info("generated token authorisation")

		logger.V(2)
		exchangedToken, err := tokenClient.Exchange(ctx, tokenId, nonce)
		if err != nil {
			return fmt.Errorf("failed to exchange token")
		}
		logger = logger.WithValues("token", exchangedToken)
		logger.V(1).Info("exchanged_token")

		// Print the token on stdout if we want to consume just this value
		fmt.Fprintf(os.Stdout, "%s\n", exchangedToken)
		return nil
	}

	return cmd
}
