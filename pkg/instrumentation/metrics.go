package instrumentation

import (
	"context"
	"fmt"
	"log"
	"net/http"

	"github.com/go-logr/logr"
	"github.com/spf13/pflag"
	"go.opencensus.io/plugin/ocgrpc"
	"go.opencensus.io/stats/view"

	"contrib.go.opencensus.io/exporter/prometheus"
	"contrib.go.opencensus.io/integrations/ocsql"
)

const (
	DefaultMetricsAddress string = ":9091"
	DefaultNamespace      string = "default"
)

type Flags struct {
	metricsAddress *string
}

func (f *Flags) GetMetricsAddress() string {
	return *f.metricsAddress
}

func (f *Flags) RunMetricsServer(ctx context.Context, logger logr.Logger) error {
	pe, err := prometheus.NewExporter(prometheus.Options{
		Namespace: DefaultNamespace,
	})
	if err != nil {
		log.Fatalf("Failed to create Prometheus exporter: %v", err)
	}
	view.RegisterExporter(pe)
	if err := view.Register(ocgrpc.DefaultClientViews...); err != nil {
		return fmt.Errorf("Failed to register ocgrpc client views: %w", err)
	}
	if err := view.Register(ocgrpc.DefaultServerViews...); err != nil {
		return fmt.Errorf("Failed to register ocgrpc server views: %w", err)
	}
	if err := view.Register(ocsql.DefaultViews...); err != nil {
		return fmt.Errorf("Failed to register ocsql client views: %w", err)
	}

	logger.Info("Registered all default stats views")

	mux := http.NewServeMux()
	mux.Handle("/metrics", pe)
	if err := http.ListenAndServe(f.GetMetricsAddress(), mux); err != nil {
		return fmt.Errorf("failed to run prometheus exporter: %w", err)
	}

	return nil
}

func SetupFlags(flags *pflag.FlagSet) *Flags {
	return &Flags{
		metricsAddress: flags.String(
			"metrics-address",
			DefaultMetricsAddress,
			"Port to listen on for metrics requests (prometheus)",
		),
	}
}
