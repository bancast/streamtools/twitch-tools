package listen

import (
	"context"
	"fmt"

	"github.com/go-logr/logr"
	dbResponses "gitlab.com/bancast/streamtools/twitch-tools/pkg/database/responses"
	grpcUtils "gitlab.com/bancast/streamtools/twitch-tools/pkg/grpc"
	"gitlab.com/bancast/streamtools/twitch-tools/pkg/responses"
	protoResponses "gitlab.com/bancast/streamtools/twitch-tools/protos/twitch/responses"
	"google.golang.org/grpc"
	"google.golang.org/grpc/codes"
)

type ResponsesConfigurationServer struct {
	protoResponses.UnimplementedResponsesConfigurationServer
	grpcUtils.DisabledAuth
	logger logr.Logger
	store  responses.Store
}

func (w ResponsesConfigurationServer) Create(ctx context.Context, req *protoResponses.CreateRequest) (*protoResponses.CreateResponse, error) {
	id, err := w.store.Create(
		ctx,
		req.TwitchId,
		dbResponses.NewResponse(
			"",
			int(req.Response.Priority),
			req.Response.Pattern,
			protoToContentType(req.Response.ContentType),
			req.Response.Content,
		),
	)
	if err != nil {
		return nil, grpc.Errorf(codes.Internal, "failed to create response: %s", err)
	}

	return &protoResponses.CreateResponse{Id: id}, nil
}

func (w ResponsesConfigurationServer) List(ctx context.Context, req *protoResponses.ListRequest) (*protoResponses.ListResponse, error) {
	responsesList, err := w.store.List(ctx, req.TwitchId)
	if err != nil {
		return nil, grpc.Errorf(codes.Internal, "failed to list responses: %s", err)
	}

	outResponses := make([]*protoResponses.Response, 0, len(responsesList))
	for _, response := range responsesList {
		outResponses = append(outResponses, &protoResponses.Response{
			Id:          response.GetID(),
			Priority:    int64(response.GetPriority()),
			Pattern:     response.GetPattern(),
			ContentType: contentTypeToProto(response.GetContentType()),
			Content:     response.GetContent(),
		})
	}

	return &protoResponses.ListResponse{
		Responses: outResponses,
	}, nil
}

func (w ResponsesConfigurationServer) Read(ctx context.Context, req *protoResponses.ReadRequest) (*protoResponses.Response, error) {
	response, err := w.store.Read(ctx, req.Id)
	if err != nil {
		return nil, grpc.Errorf(codes.Internal, "failed to read response: %s", err)
	}

	return &protoResponses.Response{
		Id:          response.GetID(),
		Priority:    int64(response.GetPriority()),
		Pattern:     response.GetPattern(),
		ContentType: contentTypeToProto(response.GetContentType()),
		Content:     response.GetContent(),
	}, nil
}

func (w ResponsesConfigurationServer) Update(ctx context.Context, req *protoResponses.UpdateRequest) (*protoResponses.UpdateResponse, error) {
	w.logger.V(3).Info("updating response", "id", req.Response.Id, "response", fmt.Sprintf("%#v", req.Response))
	err := w.store.Update(
		ctx,
		dbResponses.NewResponse(
			req.Response.Id,
			int(req.Response.Priority),
			req.Response.Pattern,
			protoToContentType(req.Response.ContentType),
			req.Response.Content,
		),
	)
	if err != nil {
		return nil, grpc.Errorf(codes.Internal, "failed to update response: %s", err)
	}

	return &protoResponses.UpdateResponse{}, nil
}

func protoToContentType(contentTypeIn protoResponses.Response_ContentType) responses.ContentType {
	switch contentTypeIn {
	case protoResponses.Response_STRING:
		return responses.ContentTypeString
	case protoResponses.Response_FORMATTED_STRING:
		return responses.ContentTypeFormatString
	case protoResponses.Response_JSONNET:
		return responses.ContentTypeJsonnet
	}

	return "invalid"
}

func contentTypeToProto(contentTypeIn responses.ContentType) protoResponses.Response_ContentType {
	switch contentTypeIn {
	case responses.ContentTypeString:
		return protoResponses.Response_STRING
	case responses.ContentTypeFormatString:
		return protoResponses.Response_FORMATTED_STRING
	case responses.ContentTypeJsonnet:
		return protoResponses.Response_JSONNET
	}

	return -1
}

func (w ResponsesConfigurationServer) Delete(ctx context.Context, req *protoResponses.DeleteRequest) (*protoResponses.DeleteResponse, error) {
	err := w.store.Delete(ctx, req.Id)
	if err != nil {
		return nil, grpc.Errorf(codes.Internal, "failed to delete response: %s", err)
	}

	return &protoResponses.DeleteResponse{}, nil
}
