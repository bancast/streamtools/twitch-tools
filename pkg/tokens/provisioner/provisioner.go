package provisioner

import (
	"context"
	"fmt"
	"time"

	"github.com/golang-jwt/jwt"

	"gitlab.com/bancast/streamtools/twitch-tools/pkg/tokens"
)

var _ tokens.Provisioner = &HMACProvisioner{}

func NewProvisioner(_ context.Context, password string) *HMACProvisioner {
	return &HMACProvisioner{
		password: password,
	}
}

type HMACProvisioner struct {
	password string
}

type StandardClaimsWithScopes struct {
	jwt.StandardClaims
	Scopes []string `json:"scopes,omitempty"`
}

func (p *HMACProvisioner) New(ctx context.Context, options tokens.Options) (string, error) {
	issueTime := time.Now().UTC().Unix()
	token := jwt.NewWithClaims(
		jwt.SigningMethodHS256,
		StandardClaimsWithScopes{
			StandardClaims: jwt.StandardClaims{
				Id:      options.TokenID,
				Subject: options.UserID,
				// Audience:  string,
				Issuer:    "identity.streamtools.dev",
				IssuedAt:  issueTime,
				NotBefore: issueTime,
			},
			Scopes: options.Scopes,
		},
	)

	// Sign and get the complete encoded token as a string using the secret
	tokenString, err := token.SignedString([]byte(p.password))
	if err != nil {
		return "", fmt.Errorf("failed to create signed token")
	}
	return tokenString, nil
}

func (p *HMACProvisioner) Validate(ctx context.Context, tokenString string, scopes []tokens.Scope) (tokens.Token, error) {
	token, err := jwt.ParseWithClaims(tokenString, &StandardClaimsWithScopes{}, func(token *jwt.Token) (interface{}, error) {
		return []byte(p.password), nil
	})
	if err != nil {
		return nil, fmt.Errorf("failed to parse token: %w", err)
	}

	claims, ok := token.Claims.(*StandardClaimsWithScopes)
	if !(ok && token.Valid) {
		return nil, fmt.Errorf("invalid token")
	}

	claimToken := Token(*claims)

	err = tokens.ValidateScopes(claimToken, scopes)
	if err != nil {
		return nil, fmt.Errorf("failed to validate scopes: %w", err)
	}

	return claimToken, nil
}
