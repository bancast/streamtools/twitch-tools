interface Response {
    id: string;
    priority: number;
    pattern: string;
    contentType: string;
    content: string;
};

export default class Client {
    baseURL: string = "https://responder.local.streamtools.dev"
    token: string

    constructor(baseURL?: string, token: string) {
        if (baseURL !== undefined) {
            this.baseURL = baseURL
        }

        this.token = token
    }

    create(twitch_id: string, response: Response) {
        return fetch(
            `${this.baseURL}/v1/responses`, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${this.token}`,
            },
            body: JSON.stringify({
                twitch_id: twitch_id,
                response: response,
            })
        }
        )
            .then(resp => resp.json())
            .catch(console.error)
    }

    update(response: Response) {
        return fetch(
            `${this.baseURL}/v1/responses/${response.id}`, {
            method: 'PUT',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${this.token}`,
            },
            body: JSON.stringify({ response: response })
        })
            .then(resp => resp.json())
            .catch(console.error)
    }

    list(twitch_id: string) {
        return fetch(
            `${this.baseURL}/v1/responses?twitch_id=${twitch_id}`, {
            method: 'GET',
            headers: {
                'Authorization': `Bearer ${this.token}`,
            },
        })
            .then(response => response.json())
            .catch(console.error)
    }

    delete(response: Response) {
        return fetch(`${this.baseURL}/v1/responses/${response.id}`, {
            method: 'DELETE',
            headers: {
                'Authorization': `Bearer ${this.token}`,
            },
        })
            .then(resp => resp.json())
            .catch(console.error)
    }
}
