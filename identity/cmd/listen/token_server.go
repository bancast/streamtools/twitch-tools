package listen

import (
	"context"
	"time"

	"github.com/go-logr/logr"
	"google.golang.org/grpc"
	"google.golang.org/grpc/codes"
	"google.golang.org/protobuf/types/known/timestamppb"

	grpcUtils "gitlab.com/bancast/streamtools/twitch-tools/pkg/grpc"
	"gitlab.com/bancast/streamtools/twitch-tools/pkg/tokens"
	"gitlab.com/bancast/streamtools/twitch-tools/protos/identity"
)

const (
	DefaultTokenLifetime time.Duration = 24 * time.Hour
)

type TokenProvisionerServer struct {
	identity.UnimplementedTokenProvisionerServer
	grpcUtils.DisabledAuth
	controller tokens.Controller
	logger     logr.Logger
}

func (t TokenProvisionerServer) Create(ctx context.Context, req *identity.TokenCreateRequest) (*identity.TokenCreateResponse, error) {
	createOptions := tokens.CreateOptions{
		User:     req.User,
		Nonce:    req.Nonce,
		Name:     req.Name,
		Lifetime: time.Duration(req.Lifetime),
		Scopes:   req.Scopes,
		Comment:  req.Comment,
	}

	// Set a default for the lifetime if unset
	if createOptions.Lifetime == 0 {
		createOptions.Lifetime = DefaultTokenLifetime
	}

	id, err := t.controller.Create(ctx, &createOptions)
	if err != nil {
		t.logger.Error(err, "failed to create token")
		return nil, grpc.Errorf(codes.Internal, "failed to create token")
	}

	return &identity.TokenCreateResponse{
		Id: id,
	}, nil
}

func (t TokenProvisionerServer) Exchange(ctx context.Context, req *identity.TokenExchangeRequest) (*identity.TokenExchangeResponse, error) {
	token, err := t.controller.Exchange(ctx, req.Id, req.Nonce)
	if err != nil {
		t.logger.Error(err, "failed to exchange token", "tid", req.Id)
		return nil, grpc.Errorf(codes.Internal, "failed to exchange token")
	}

	return &identity.TokenExchangeResponse{
		Token: token,
	}, nil
}

func (t TokenProvisionerServer) AuthFuncOverride(ctx context.Context, _ string) (context.Context, error) {
	// Return the same context since these methods should all open to all
	return ctx, nil
}

type TokenManagementServer struct {
	identity.UnimplementedTokenManagementServer
	controller tokens.Controller
	logger     logr.Logger
}

func (t TokenManagementServer) List(ctx context.Context, req *identity.TokenListRequest) (*identity.TokenListResponse, error) {
	ctx, cancel := context.WithCancel(ctx)
	defer cancel()

	tokens, err := t.controller.List(ctx)
	if err != nil {
		t.logger.Error(err, "failed to list tokens")
		return nil, grpc.Errorf(codes.Internal, "failed to list tokens")
	}
	return &identity.TokenListResponse{
		Tokens: mapTokens(tokens),
	}, nil
}

func mapTokens(tks []tokens.Token) []*identity.TokenInfo {
	itks := make([]*identity.TokenInfo, 0, len(tks))
	for _, token := range tks {
		itks = append(itks, tokenToProto(token))
	}
	return itks
}

func tokenToProto(token tokens.Token) *identity.TokenInfo {
	return &identity.TokenInfo{
		User:      token.GetUserID(),
		Name:      token.GetName(),
		Expiry:    timestamppb.New(token.GetExpiry().UTC()),
		CreatedAt: timestamppb.New(token.GetCreatedAt().UTC()),
		LastUsed:  timestamppb.New(token.GetLastUsedAt().UTC()),
		Scopes:    token.GetScopes(),
		Comment:   token.GetComment(),
		Exchanged: token.GetExchanged(),
	}
}

func (t TokenManagementServer) Read(ctx context.Context, req *identity.TokenReadRequest) (*identity.TokenReadResponse, error) {
	ctx, cancel := context.WithCancel(ctx)
	defer cancel()

	token, err := t.controller.Read(ctx, req.Id)
	if err != nil {
		t.logger.Error(err, "failed to read token")
		return nil, grpc.Errorf(codes.NotFound, "failed to read token: %s")
	}
	return &identity.TokenReadResponse{
		Token: tokenToProto(token),
	}, nil
}

func (t TokenManagementServer) Refresh(ctx context.Context, req *identity.TokenRefreshRequest) (*identity.TokenRefreshResponse, error) {
	t.controller.Refresh(ctx)
	return nil, grpc.Errorf(codes.Unimplemented, "unimplemented")
}
