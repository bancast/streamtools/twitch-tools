import { BrowserRouter as Router, Route, Link, Redirect, Switch } from "react-router-dom";
import { useForm } from "react-hook-form";
import createPersistedState from 'use-persisted-state';
import { Fragment, useState, useEffect, useContext, createContext } from "react";
import Client from './clients/responses.ts';
import TokenClient from './clients/identity.ts';
import './App.css';

const tokenClient = new TokenClient("https://identity.local.streamtools.dev", "https://identity.staging.streamtools.dev");

// const baseURL = "http://localhost:8081";
const baseURL = "https://twitch.responder.local.streamtools.dev";

const useNonceState = createPersistedState('_nonce');
const useTokenState = createPersistedState('_token');
const authContext = createContext();

function App() {
    return (
        <Router>
            <ProvideAuth>
                <NavBar />
                <div className="container-fluid">
                    <main className="row">
                        <nav className="sidebar col-3 flex-column">
                            <ul className="nav nav-pills">
                                <li className="nav-item"><Link to={`/responses`} className="nav-link">Responses</Link></li>
                            </ul>
                        </nav>
                        <section className="content col-9">
            <Switch>
                                <Route path="/" exact component={HomeOrLogin} />
                                <AuthenticatedRoutes>
                                    <Route path="/responses" exact component={Responses} />
                                </AuthenticatedRoutes>
                                <Route render={() => <><h1>404: page not found</h1><img alt="" src="https://http.cat/404.jpg" /></>} />
                            </Switch>
                        </section>
                    </main>
                </div>
            </ProvideAuth>
        </Router>
    );
}

// Provider component that wraps your app and makes auth object ...
// ... available to any child component that calls useAuth().
export function ProvideAuth({ children }: ChildrenProps) {
    const auth = useProvideAuth();
    return <authContext.Provider value={auth}>{children}</authContext.Provider>;
}

// Hook for child components to get the auth object ...
// ... and re-render when it changes.
export const useAuth = () => {
    return useContext(authContext);
};

function useProvideAuth() {
    const [nonce, setNonce] = useNonceState(null);
    const [token, setToken] = useTokenState(null);

    const isAuthenticated = token !== null;

    const login = () => {
        tokenClient
            .login()
            .then(resp => {
                setNonce(resp.nonce);
                window.location.href = resp.url;
            });
    }

    const exchange = () => {
        const token_id = window.location.hash.replaceAll("#", "");
        tokenClient
            .exchange(nonce, token_id)
            .then(token => {
                setNonce(null);
                setToken(token);
                window.location.hash = "";
            });
    }

    const logout = () => {
        setNonce(null);
        setToken(null);
    }

    // Try to exchange if we can
    useEffect(() => {
        if (nonce !== null && window.location.hash !== "") {
            exchange()
        }
    }, []);

    return {
        token,
        login,
        logout,
    }
}

type ChildrenProps = {
    children?: React.ReactNode;
};

function AuthenticatedRoutes({ children }: ChildrenProps) {
    const auth = useAuth();

    if (auth.token === null) {
        return (<Redirect to='/' />)
    }

    return (<>{children}</>)
}

function HomeOrLogin() {
    const auth = useAuth();

    if (auth.token === null) {
	return (<Login />)
    }

    return (<Home />)
}

function Home() {
    return (
        <>
            <h1>Welcome to the homepage!</h1>
        </>
    )
}

function Login() {
    const auth = useAuth();

    return (
        <div>
            <h1>Login</h1>
            <div>
                <button className="btn twitch" onClick={() => auth.login()}>Login with Twitch</button>
            </div>
        </div>
    )
}

function NavBar() {
    const auth = useAuth();

    return (
        <div className="container-fluid">
            <div className="row">
                <div className="col-9">
                    <Logo />
                </div>
                <div className="col-3 p-2 nav justify-content-end">
                    {
                        !auth.token ? (
                            <button className="btn twitch" onClick={() => auth.login()}>Login with Twitch</button>
                        ) : (
                            <button className="btn logout" onClick={() => auth.logout()}>Logout</button>
                        )
                    }
                </div>
            </div>
        </div>
    );
}

function Logo() {
    return (
        <Link to="/" className="nav-link"><h1>StreamTools</h1></Link>
    )
}

function Responses() {
    const auth = useAuth();
    const responsesClient = new Client(baseURL, auth.token);

    const [users, setUsers] = useState([]);
    const [selectedUser, setSelectedUser] = useState(null);
    const [responses, setResponses] = useState([]);
    const [globQuery, setGlobQuery] = useState('');
    const [matchQuery, setMatchQuery] = useState('');

    // Sort the responses by priority,id
    responses.sort((a, b) => {
        if (a.priority !== b.priority) {
            return b - a
        }
        if (a.pattern !== b.pattern) {
            if (a.pattern < b.pattern) {
                return -1
            }
            return 1
        }

        if (a.id !== b.id) {
            if (a.id < b.id) {
                return -1
            }
            return 1
        }

        return 0
    })

    let responseFilters = [
        globSearchFilter(globQuery),
        patternMatchFilter(matchQuery),
    ];
    let filteredResponses = responses.filter(r => responseFilters.every(f => f(r)))

    const refreshResponses = () => responsesClient
        .list(selectedUser.twitchId)
        .then(data => setResponses(data.responses))
        .catch(error => console.log(error.message))

    const refreshUsersList = () => tokenClient.self(auth.token).then(resp => {
        setUsers([resp]);
        setSelectedUser(resp);
    });

    useEffect(() => {
        refreshUsersList()
    }, []);

    useEffect(() => {
        if (selectedUser === null) {
            return
        }
        refreshResponses()
    }, [selectedUser])

    return (
        <>
            <UserSelectForm users={users} selectUser={setSelectedUser} />
            <button className="btn btn-primary" onClick={() => refreshUsersList()}>Refresh Users list</button>
            {selectedUser !== null ? (
                <>
                    <h1>Responses for <span className="text-muted">{selectedUser.twitchId}</span></h1>
                    <button className="btn btn-primary" onClick={() => refreshResponses()}>Refresh</button>
                    <input className="form-control" type="text" onChange={(e) => setGlobQuery(e.target.value)} placeholder="Search by content" value={globQuery} />
                    <input className="form-control" type="text" onChange={(e) => setMatchQuery(e.target.value)} placeholder="Search by match" value={matchQuery} />
                    <ResponsesList responsesClient={responsesClient} responses={filteredResponses} twitch_id={selectedUser.twitchId} refreshResponses={() => refreshResponses()} /></>
            ) : (
                <></>
            )}
        </>
    )
}

function UserSelectForm({ users, selectUser }) {
    return (
        <form>
            <select>
                {
                    users.map((user) => (<option key={user.id}>{user.id}</option>))
                }
            </select>
        </form>
    )
}

function globSearchFilter(query) {
    return (response) => !query ||
        response.pattern.toLowerCase().includes(query) ||
        response.content.toLowerCase().includes(query)
}

function patternMatchFilter(query) {
    return (response) => !query || (new RegExp(response.pattern)).test(query)
}

function ResponsesList({ responses, ...props }) {

    // if (responses.length() === 0) {
    //     return <Fragment><NoResponses /><Fragment />
    // }

    return (
        <Fragment>
            <div className="list-responses row row-cols-1 row-cols-md-3 row-cols-lg-4 g-4">
                <AddResponseForm {...props} />
                {
                    responses.map(
                        response => (
				<Response key={response.id} response={response} {...props} />
                        )
                    )
                }
            </div>
        </Fragment>
    )
}

function AddResponseForm({ twitch_id, responsesClient }) {
    const { register, handleSubmit, reset, formState: { errors } } = useForm();
    console.log(errors);

    return (
        <Fragment>
            <div className="col">
                <div className="card">
                    <div className="card-body">
                        <h5 className="card-title text-muted">Build-a-Response</h5>
                        <form onSubmit={handleSubmit(response => responsesClient.create(twitch_id, response).then(reset))}>
                            <div className="mb-3 col-auto">
                                <label htmlFor="priority" className="form-label">Priority</label>
                                <input type="number" className="form-control" placeholder="!hello" {...register("priority", { required: true, valueAsNumber: true })} defaultValue={100} />
                            </div>
                            <div className="mb-3 col-auto">
                                <label htmlFor="pattern" className="form-label">Pattern</label>
                                <input type="text" className="form-control" placeholder="!hello" {...register("pattern", { required: true })} />
                            </div>
                            <div className="mb-3 col-auto">
                                <label htmlFor="contentType" className="form-label">Content Type</label>
                                <select defaultValue={'STRING'} className="form-control" {...register("contentType", { required: true })}>
                                    <option value="STRING">String</option>
                                    <option value="FORMAT_STRING">Format String</option>
                                    <option value="JSONNET">Jsonnet</option>
                                </select>
                            </div>
                            <div className="mb-3 col-auto">
                                <label htmlFor="content" className="form-label">Content</label>
                                <textarea className="form-control" placeholder="!hello" {...register("content", { required: true })} />
                            </div>

                            <div className="btn-group" role="group">
                                <input type="submit" value="Save" className="btn btn-primary" />
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </Fragment>
    )
}


function Response({ response, refreshResponses, responsesClient, twitch_id }) {
    const { register, handleSubmit, formState: { errors } } = useForm();
    console.log(errors);

    return (
        <Fragment>
            <div className="col">
                <div className="card">
                    <div className="card-body">
                        <h5 className="card-title text-muted">{response.id}</h5>
                        <form onSubmit={handleSubmit(response => responsesClient.update(response))}>
                            <input type="text" className="d-none" {...register("id")} defaultValue={response.id} />
                            <div className="mb-3 col-auto">
                                <label htmlFor="priority" className="form-label">Priority</label>
                                <input type="number" className="form-control" placeholder="!hello" {...register("priority", { required: true, valueAsNumber: true })} defaultValue={response.priority} />
                            </div>
                            <div className="mb-3 col-auto">
                                <label htmlFor="pattern" className="form-label">Pattern</label>
                                <input type="text" className="form-control" placeholder="!hello" {...register("pattern", { required: true })} defaultValue={response.pattern} />
                            </div>
                            <div className="mb-3 col-auto">
                                <label htmlFor="contentType" className="form-label">Content Type</label>
                                <select defaultValue={response.contentType} className="form-control" {...register("contentType", { required: true })}>
                                    <option value="STRING">String</option>
                                    <option value="FORMAT_STRING">Format String</option>
                                    <option value="JSONNET">Jsonnet</option>
                                </select>
                            </div>
                            <div className="mb-3 col-auto">
                                <label htmlFor="content" className="form-label">Content</label>
                                <textarea className="form-control" placeholder="!hello" {...register("content", { required: true })} defaultValue={response.content} />
                            </div>

                            <div className="btn-group" role="group">
                                <input type="submit" value="Save" className="btn btn-primary" />
                                <button className="btn btn-success" onClick={handleSubmit(response => responsesClient.create(twitch_id, response).then(refreshResponses))}>Duplicate</button>
                                <button className="btn btn-danger" onClick={handleSubmit(response => responsesClient.delete(response).then(refreshResponses))}>Delete</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </Fragment>
    )
}



// function NoResponses() {
//     return (
// 	<Fragment><h3>No responses set</h3></Fragment>
//     )
// }

export default App;
