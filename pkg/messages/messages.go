package messages

import (
	"errors"

	"gitlab.com/bancast/streamtools/twitch-tools/protos/twitch/messages"
)

var (
	MessageTypeUnknownError  = errors.New("message type is not known")
	MessageTypeUnknownString = "UNKNOWN"
)

func MessageType(msg *messages.TwitchMessage) (string, error) {
	message := msg.GetMsg()
	if message == nil {
		return MessageTypeUnknownString, MessageTypeUnknownError
	}

	switch tMessage := message.(type) {
	case *messages.TwitchMessage_WhisperMessage:
		return tMessage.WhisperMessage.Type.String(), nil
	case *messages.TwitchMessage_PrivateMessage:
		return tMessage.PrivateMessage.Type.String(), nil
	case *messages.TwitchMessage_ClearChatMessage:
		return tMessage.ClearChatMessage.Type.String(), nil
	case *messages.TwitchMessage_ClearMessage:
		return tMessage.ClearMessage.Type.String(), nil
	case *messages.TwitchMessage_RoomStateMessage:
		return tMessage.RoomStateMessage.Type.String(), nil
	case *messages.TwitchMessage_UserNoticeMessage:
		return tMessage.UserNoticeMessage.Type.String(), nil
	case *messages.TwitchMessage_UserStateMessage:
		return tMessage.UserStateMessage.Type.String(), nil
	case *messages.TwitchMessage_GlobalUserStateMessage:
		return tMessage.GlobalUserStateMessage.Type.String(), nil
	case *messages.TwitchMessage_NoticeMessage:
		return tMessage.NoticeMessage.Type.String(), nil
	case *messages.TwitchMessage_UserJoinMessage:
		return "USER_JOIN", nil
	case *messages.TwitchMessage_UserPartMessage:
		return "USER_PART", nil
	case *messages.TwitchMessage_ReconnectMessage:
		return tMessage.ReconnectMessage.Type.String(), nil
	}

	return MessageTypeUnknownString, MessageTypeUnknownError
}
