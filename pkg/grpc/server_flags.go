package grpc

import (
	"context"

	grpc_auth "github.com/grpc-ecosystem/go-grpc-middleware/auth"
	"github.com/grpc-ecosystem/grpc-gateway/v2/runtime"
	"github.com/spf13/pflag"
	"gitlab.com/bancast/streamtools/twitch-tools/pkg/tokens"
	"gitlab.com/bancast/streamtools/twitch-tools/pkg/tokens/provisioner"
	"go.opencensus.io/plugin/ocgrpc"
	"google.golang.org/grpc"
	"google.golang.org/grpc/reflection"
)

const (
	DefaultGrpcPort int = 9090
	DefaultHttpPort int = 8081
)

type ServerFlags struct {
	grpcPort    *int
	httpPort    *int
	corsOrigins *[]string
	authSecret  *string
	provisioner tokens.Provisioner
	serveMux    *runtime.ServeMux
	grpcServer  *grpc.Server
	options     Options
}

func (f ServerFlags) HttpPort() int {
	return *f.httpPort
}

func (f ServerFlags) GrpcPort() int {
	return *f.grpcPort
}

func (f ServerFlags) CorsOrigins() []string {
	return *f.corsOrigins
}

func (f *ServerFlags) HTTPMux() *runtime.ServeMux {
	if f.serveMux == nil {
		f.serveMux = runtime.NewServeMux()
	}

	return f.serveMux
}

func (f *ServerFlags) TokenProvisioner() tokens.Provisioner {
	if f.provisioner == nil {
		f.provisioner = provisioner.NewProvisioner(context.Background(), *f.authSecret)
	}
	return f.provisioner
}

func (f *ServerFlags) GRPCServer() *grpc.Server {
	if f.grpcServer == nil {
		provisioner := tokens.ContextProvisioner{Provisioner: f.TokenProvisioner()}
		authFunc := OverrideAuthFunc(MakePermissiveAuthFunc(
			provisioner,
			f.options.AllowedMethods,
		))

		// Instrument our GRPC server
		f.grpcServer = grpc.NewServer(
			grpc.StatsHandler(&ocgrpc.ServerHandler{}),
			grpc.StreamInterceptor(grpc_auth.StreamServerInterceptor(authFunc)),
			grpc.UnaryInterceptor(grpc_auth.UnaryServerInterceptor(authFunc)),
		)

		// Enable reflection for the server to allow for tooling such as grpcui
		reflection.Register(f.grpcServer)
	}

	return f.grpcServer
}

func SetupServerFlags(flags *pflag.FlagSet) *ServerFlags {
	return &ServerFlags{
		grpcPort: flags.Int(
			"grpc-port",
			DefaultGrpcPort,
			"Port on which to expose our GRPC servers",
		),
		httpPort: flags.Int(
			"http-port",
			DefaultHttpPort,
			"Port on which to expose our HTTP gateway to our GRPC server",
		),
		corsOrigins: flags.StringSlice(
			"cors-origins",
			[]string{},
			"Origins to allow requests from",
		),
		authSecret: flags.String(
			"auth-secret",
			"change-me",
			"Secret to use for provisioning tokens",
		),
	}
}
