package jsonnetenv

import (
	"context"

	jsonnet "github.com/google/go-jsonnet"
	"github.com/google/go-jsonnet/ast"
)

func WithNativeFunction(name string, params []string, function func([]interface{}) (interface{}, error)) VMOption {
	return func(_ context.Context, vm *VM) error {
		identifiers := make([]ast.Identifier, 0, len(params))
		for _, param := range params {
			identifiers = append(identifiers, ast.Identifier(param))
		}

		vm.VM.NativeFunction(&jsonnet.NativeFunction{
			Func:   function,
			Params: identifiers,
			Name:   name,
		})

		return nil
	}
}
