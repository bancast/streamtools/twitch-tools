package tokens

import (
	"context"
	"errors"
	"fmt"
	"time"
)

type Token interface {
	GetTokenID() string
	GetCreatedAt() time.Time
	GetLastUsedAt() time.Time
	GetNonce() string
	GetUserID() string

	// Time the token expires
	//
	// This is mainly for book-keeping and clean up rather than if
	// the token is actually valid
	GetExpiry() time.Time
	// The time that new versions of the token should live
	GetLifetime() time.Duration

	// Has the token been exchanged, we shouldn't be able to
	// exchange more than once
	GetExchanged() bool
	GetExchangeBy() time.Time

	// Token name
	GetName() string
	GetComment() string

	GetScopes() []string
}

type CreateOptions struct {
	User     string
	Nonce    string
	Name     string
	Lifetime time.Duration
	Scopes   []string
	Comment  string
}

type ReadOptions struct {
	ID string
}

type ExchangeOptions struct {
	ID    string
	Nonce string
}

type RefreshOptions struct {
	TokenID string
	UserID  string
}

type Store interface {
	List(context.Context) ([]Token, error)
	Create(context.Context, *CreateOptions) (string, error)
	Read(context.Context, *ReadOptions) (Token, error)
	Exchange(context.Context, *ExchangeOptions) (Transaction, error)
	Refresh(context.Context, *RefreshOptions) (Transaction, error)
}

type Options struct {
	TokenID  string
	UserID   string
	Expiry   time.Time
	Lifetime time.Duration
	Scopes   []string
}

type Transaction interface {
	Options() Options
	// Commit changes
	Complete() error
	// Rollback changes
	Error() error
}

type Provisioner interface {
	New(context.Context, Options) (string, error)
	Validate(context.Context, string, []Scope) (Token, error)
}

type Validator interface {
	Validate(context.Context, string, []Scope) (Token, error)
}

type Controller interface {
	Create(context.Context, *CreateOptions) (string, error)
	List(context.Context) ([]Token, error)
	Read(context.Context, string) (Token, error)
	Exchange(context.Context, string, string) (string, error)
	Refresh(context.Context) (string, error)
}

type Operator interface {
	Controller
	Validator
}

type TokenKey struct{}

var (
	ErrorContextTokenNotFound error = errors.New("token not in context")
	ErrorContextTokenNotValid error = errors.New("token not not valid")
)

type ContextProvisioner struct {
	Provisioner
	Scopes []Scope
}

func (cp *ContextProvisioner) SetToken(ctx context.Context, tokenString string) (context.Context, error) {
	token, err := cp.Validate(ctx, tokenString, cp.Scopes)
	if err != nil {
		return nil, fmt.Errorf("invalid token: %w", err)
	}

	return context.WithValue(ctx, TokenKey{}, token), nil
}

func (cp *ContextProvisioner) GetToken(ctx context.Context) (Token, error) {
	tokenI := ctx.Value(TokenKey{})
	if tokenI == nil {
		return nil, ErrorContextTokenNotFound
	}

	token, ok := tokenI.(Token)
	if !ok {
		return nil, ErrorContextTokenNotFound
	}

	return token, nil
}
