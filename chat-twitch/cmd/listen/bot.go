package listen

import (
	"context"
	"fmt"

	twitchIRC "github.com/gempir/go-twitch-irc/v2"
	"github.com/go-logr/logr"
	"gitlab.com/bancast/streamtools/twitch-tools/pkg/twitch"
)

// Bot create a loop that reacts to login events and spins up a new
// IRC client to twitch chat. This allows us to listen for messages on
// the channels we subscribe to, and send them off to the sink for
// processing.
func Bot(ctx context.Context, logger logr.Logger, controller *twitch.Controller, sink *TwitchSinker, loginChan chan string) {
	logger.Info("starting up bot listener loop")
	// Range over all of the login events we get. This is a simple
	// construct to allow us to easily create new listener bots. When
	// we move to a global listener we will likely want to change how
	// we do this.
	for loginEvent := range loginChan {
		twitch_id := loginEvent
		// username and channel are the same at the moment, we will
		// possibly want to split up these concepts when we have a bot
		// for the channel
		logger := logger.WithValues("user", twitch_id)

		go func() {
			ctx := SetTwitchIDContext(ctx, twitch_id)
			client, err := controller.UserClient(ctx, twitch_id)
			if err != nil {
				logger.Error(
					err, "failed to create user client",
					"twitch_id", twitch_id,
				)
				return
			}

			logger.Info("maybe refreshing token")
			err = client.MaybeRefresh(ctx)
			if err != nil {
				logger.Error(err, "failed to refresh token")
			}

			user, err := client.Self(ctx)
			if err != nil {
				logger.Error(err, "failed to get self")
				return
			}

			logger := logger.WithValues("channel", user.DisplayName, "user_login", user.Login)

			irc := twitchIRC.NewClient(user.Login, fmt.Sprintf("oauth:%s", client.Tokens.AccessToken))

			logger.V(3).Info("registering message callbacks")
			irc.OnWhisperMessage(sink.NewWhisperMessageHandler(ctx, func(_ twitchIRC.WhisperMessage) {}))
			irc.OnPrivateMessage(sink.NewPrivateMessageHandler(ctx, func(msg twitchIRC.PrivateMessage) {}))
			irc.OnClearChatMessage(sink.NewClearChatMessageHandler(ctx, func(_ twitchIRC.ClearChatMessage) {}))
			irc.OnClearMessage(sink.NewClearMessageHandler(ctx, func(_ twitchIRC.ClearMessage) {}))
			irc.OnRoomStateMessage(sink.NewRoomStateMessageHandler(ctx, func(_ twitchIRC.RoomStateMessage) {}))
			irc.OnUserNoticeMessage(sink.NewUserNoticeMessageHandler(ctx, func(_ twitchIRC.UserNoticeMessage) {}))
			irc.OnUserStateMessage(sink.NewUserStateMessageHandler(ctx, func(_ twitchIRC.UserStateMessage) {}))
			irc.OnGlobalUserStateMessage(sink.NewGlobalUserStateMessageHandler(ctx, func(_ twitchIRC.GlobalUserStateMessage) {}))
			irc.OnNoticeMessage(sink.NewNoticeMessageHandler(ctx, func(_ twitchIRC.NoticeMessage) {}))
			irc.OnUserJoinMessage(sink.NewUserJoinMessageHandler(ctx, func(_ twitchIRC.UserJoinMessage) {}))
			irc.OnUserPartMessage(sink.NewUserPartMessageHandler(ctx, func(_ twitchIRC.UserPartMessage) {}))
			irc.OnReconnectMessage(
				sink.NewReconnectMessageHandler(ctx, func(msg twitchIRC.ReconnectMessage) {
					logger.Info("received reconnect message")
					logger.Info("refreshing token")
					err := client.RefreshToken(ctx)
					if err != nil {
						logger.Error(err, "failed to refresh token")
						return
					}
					logger.Info("disconnecting from twitch chat")
					err = irc.Disconnect()
					if err != nil {
						logger.Error(err, "failed to disconnect")
					}

					logger.Info("sending login event")
					loginChan <- twitch_id
				}),
			)
			// TODO add ping processing

			logger.Info("joining channel")
			irc.Join(user.Login)

			logger.Info("connecting to twitch chat")
			err = irc.Connect()
			if err != nil {
				logger.Error(err, "failed to connect")
				return
			}

			logger.Info("connected to chat")
		}()
	}
}
