package listen

import (
	"context"
	"fmt"

	"github.com/spf13/cobra"
	gateway "gitlab.com/bancast/streamtools/twitch-tools/pkg/grpc"
	"gitlab.com/bancast/streamtools/twitch-tools/pkg/logging"
	"gitlab.com/bancast/streamtools/twitch-tools/pkg/pubsub"
	"gitlab.com/bancast/streamtools/twitch-tools/protos/webhookd"
	"google.golang.org/grpc"
)

func NewListenCommand(logger *logging.Logger) *cobra.Command {
	cmd := &cobra.Command{
		Use:   "listen",
		Short: "Listen for new webhook pushes",
	}

	pubsubFlags := pubsub.SetupFlags(cmd.PersistentFlags())
	grpcServerFlags := gateway.SetupServerFlags(cmd.PersistentFlags())

	cmd.RunE = func(cmd *cobra.Command, args []string) error {
		logger := logger.Logr()
		ctx, cancel := context.WithCancel(cmd.Context())
		defer cancel()

		ch, closeCh := pubsubFlags.MustChannel(ctx, logger)
		defer closeCh()

		logger.Info("creating webhook server")
		server := WebhookServer{channel: ch, logger: logger.WithName("api")}
		logger.Info("starting server", "server", "webhooks")
		err := grpcServerFlags.RegisterGateway(
			ctx, logger,
			func(ctx context.Context, grpcServer *grpc.Server) error {
				logger.Info("registering grpc service")
				webhookd.RegisterWebhookServiceServer(grpcServer, server)
				return nil
			},
			webhookd.RegisterWebhookServiceHandlerFromEndpoint,
		)
		if err != nil {
			return fmt.Errorf("failed to run with gateway: %w", err)
		}

		return grpcServerFlags.Listen(ctx, logger.WithName("server"))
	}

	return cmd
}
