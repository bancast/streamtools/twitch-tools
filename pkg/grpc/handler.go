package grpc

import (
	"context"
	"fmt"
	"net"
	"net/http"
	"strings"

	"github.com/go-logr/logr"
	"github.com/gobwas/glob"
	"github.com/grpc-ecosystem/grpc-gateway/v2/runtime"
	"google.golang.org/grpc"
)

func (flags *ServerFlags) RegisterGateway(ctx context.Context, logger logr.Logger, registerGrpcFunc func(context.Context, *grpc.Server) error, registerGatewayFunc func(context.Context, *runtime.ServeMux, string, []grpc.DialOption) error) error {
	err := flags.registerGrpcServer(ctx, registerGrpcFunc)
	if err != nil {
		return err
	}

	return flags.registerHTTPServer(
		ctx,
		flags.GrpcPort(),
		flags.HttpPort(),
		registerGatewayFunc,
	)
}

func (flags *ServerFlags) registerHTTPServer(ctx context.Context, grpcPort int, httpPort int, registerGatewayFunc func(context.Context, *runtime.ServeMux, string, []grpc.DialOption) error) error {
	mux := flags.HTTPMux()

	opts := []grpc.DialOption{grpc.WithInsecure()}
	err := registerGatewayFunc(
		ctx,
		mux,
		fmt.Sprintf("localhost:%d", grpcPort),
		opts,
	)
	if err != nil {
		return err
	}

	return nil
}

func (flags *ServerFlags) HTTPListen(ctx context.Context) error {
	return http.ListenAndServe(fmt.Sprintf(":%d", flags.HttpPort()), flags.wrapCors(flags.HTTPMux()))
}

func (flags *ServerFlags) CorsMatch(origin string) bool {
	for _, originGlob := range flags.CorsOrigins() {
		g := glob.MustCompile(originGlob)
		if g.Match(origin) {
			return true
		}
	}

	return false
}

func preflightHandler(w http.ResponseWriter, r *http.Request) {
	headers := []string{"Content-Type", "Accept", "Authorization"}
	w.Header().Set("Access-Control-Allow-Headers", strings.Join(headers, ","))
	methods := []string{"GET", "HEAD", "POST", "PUT", "DELETE"}
	w.Header().Set("Access-Control-Allow-Methods", strings.Join(methods, ","))
	return
}

func (flags *ServerFlags) wrapCors(h http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		if origin := r.Header.Get("Origin"); origin != "" {
			if !flags.CorsMatch(origin) {
				return
			}

			w.Header().Set("Access-Control-Allow-Origin", origin)
			if r.Method == "OPTIONS" && r.Header.Get("Access-Control-Request-Method") != "" {
				preflightHandler(w, r)
				return
			}
		}
		h.ServeHTTP(w, r)
	})
}

func (flags *ServerFlags) registerGrpcServer(ctx context.Context, registerGrpcFunc func(context.Context, *grpc.Server) error) error {
	grpcServer := flags.GRPCServer()

	err := registerGrpcFunc(ctx, grpcServer)
	if err != nil {
		return err
	}
	return nil
}

func (flags *ServerFlags) GRPCListen(ctx context.Context) error {
	lis, err := net.Listen("tcp", fmt.Sprintf(":%d", flags.GrpcPort()))
	if err != nil {
		return err
	}

	grpcServer := flags.GRPCServer()
	return grpcServer.Serve(lis)
}

func (flags *ServerFlags) Listen(ctx context.Context, logger logr.Logger) error {
	ctx, cancel := context.WithCancel(ctx)
	defer cancel()

	go func() {
		err := flags.HTTPListen(ctx)
		if err != nil {
			logger.Error(err, "HTTP listen failed")
		}
		cancel()
	}()

	go func() {
		err := flags.GRPCListen(ctx)
		if err != nil {
			logger.Error(err, "GRPC listen failed")
		}
		cancel()
	}()

	// Wait for this to be done
	<-ctx.Done()
	return nil
}
