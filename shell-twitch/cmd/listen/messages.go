package listen

import (
	"context"
	"fmt"
	"os/exec"
	"regexp"
	"strconv"
	"time"

	"github.com/go-logr/logr"
	"gitlab.com/bancast/streamtools/twitch-tools/protos/twitch/chat"
	"gitlab.com/bancast/streamtools/twitch-tools/protos/twitch/messages"
	"go.opencensus.io/stats"
	"go.opencensus.io/stats/view"
	"go.opencensus.io/tag"

	messageHelpers "gitlab.com/bancast/streamtools/twitch-tools/pkg/messages"
)

var (
	keyMessageType = tag.MustNewKey("message-type")
	keyExitCode    = tag.MustNewKey("exit-code")
	keyChannel     = tag.MustNewKey("channel")
	keyDisplayName = tag.MustNewKey("display-name")

	messageLatency     = stats.Int64("latency", "The latency in milliseconds", "ms")
	MessageLatencyView = &view.View{
		Name:        "messages/latency",
		Measure:     messageLatency,
		Description: "The distribution of the latencies",
		TagKeys:     []tag.Key{keyMessageType, keyExitCode, keyChannel, keyDisplayName},
		Aggregation: view.Distribution(0, 1, 2, 5, 10, 25, 100, 200, 400, 800, 10000),
	}

	MessageTotalView = &view.View{
		Name:        "messages/total",
		Measure:     messageLatency,
		Description: "The number of messages that were received",
		TagKeys:     []tag.Key{keyMessageType, keyExitCode, keyChannel, keyDisplayName},
		Aggregation: view.Count(),
	}
)

// ReadMessages reads and processes the messages, returning a response if required
func ReadMessages(ctx context.Context, logger logr.Logger, twitchChat chat.TwitchChatClient, channel string, pattern string, args []string, msgs <-chan *messages.TwitchMessage) error {
	cpattern, err := regexp.Compile(pattern)
	if err != nil {
		return fmt.Errorf("failed to compile pattern: %w", err)
	}

	for msg := range msgs {
		logger := logger.WithValues("twitch_id", msg.TwitchId)
		baseMessage := msg.GetMsg()
		if baseMessage == nil {
			continue
		}

		msgType, _ := messageHelpers.MessageType(msg)
		ctx, err := tag.New(
			ctx,
			tag.Upsert(keyMessageType, msgType),
		)
		if err != nil {
			logger.Error(err, "failed to set tags on context")
			continue
		}

		startTime := time.Now()
		recordMetrics := func(ctx context.Context, code int) {
			ctx, _ = tag.New(
				ctx,
				tag.Upsert(keyExitCode, strconv.Itoa(code)),
			)
			stats.Record(
				ctx,
				messageLatency.M(
					time.
						Now().
						Sub(startTime).
						Milliseconds(),
				),
			)
		}

		if privMsg, ok := baseMessage.(*messages.TwitchMessage_PrivateMessage); ok {
			ctx, err := tag.New(
				ctx,
				tag.Upsert(keyChannel, privMsg.PrivateMessage.Channel),
				tag.Upsert(keyDisplayName, privMsg.PrivateMessage.User.DisplayName),
			)
			if err != nil {
				logger.Error(err, "failed to set tags on context")
				continue
			}
			// Filter by channel
			if privMsg.PrivateMessage.Channel != channel {
				continue
			}

			content := privMsg.PrivateMessage.Msg
			// Skip messages that don't match pattern we're looking for
			if !cpattern.MatchString(content) {
				recordMetrics(ctx, 0)
				continue
			}

			cmd := exec.CommandContext(ctx, args[0], args[1:]...)
			out, err := cmd.Output()
			if err != nil {
				recordMetrics(ctx, err.(*exec.ExitError).ExitCode())
				return fmt.Errorf("failed to read output: %w", err)
			}
			logger.Info(
				"message matched",
				"msg", content,
				"user", privMsg.PrivateMessage.User.DisplayName,
				"channel", privMsg.PrivateMessage.Channel,
			)
			recordMetrics(ctx, 0)

			// Send the response to chat
			if len(out) > 0 {
				logger.Info("response created")
				ctx, cancel := context.WithTimeout(ctx, 5*time.Second)

				logger.Info("sending response to chat-twitch")
				_, err = twitchChat.Send(ctx, &chat.TwitchChatRequest{
					TwitchId: msg.TwitchId,
					Body:     string(out),
				})
				if err != nil {
					logger.Error(err, "failed to send response to chat-twitch")
					cancel()
					continue
				}

				logger.Info("sent response")
				cancel()
			}
			continue
		}
	}

	return nil
}
