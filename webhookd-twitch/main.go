package main

import "gitlab.com/bancast/streamtools/twitch-tools/webhookd-twitch/cmd"

func main() {
	cmd.Execute()
}
