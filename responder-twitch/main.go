package main

import "gitlab.com/bancast/streamtools/twitch-tools/responder-twitch/cmd"

func main() {
	cmd.Execute()
}
