package database

import (
	"context"
	"fmt"

	"github.com/google/uuid"
	"github.com/jmoiron/sqlx"
	"gitlab.com/bancast/streamtools/twitch-tools/pkg/webhooks"
)

func NewWebhook(id string, url string, secret string) *Webhook {
	uid, _ := uuid.Parse(id)
	return &Webhook{
		ID:     uid,
		URL:    url,
		Secret: secret,
	}
}

type Webhook struct {
	ID              uuid.UUID `json:"id" db:"id"`
	URL             string    `json:"id" db:"url"`
	Secret          string    `json:"secret" db:"secret"`
	WebhookConfigID uuid.UUID `json:"webhook_config_id" db:"webhook_config_id"`
}

func (wh Webhook) GetID() string {
	return wh.ID.String()
}

func (wh Webhook) GetURL() string {
	return wh.URL
}

func (wh Webhook) GetSecret() string {
	return wh.Secret
}

var _ webhooks.Store = &WebhookStore{}

func NewWebhookStore(ctx context.Context, db *sqlx.DB) (*WebhookStore, error) {
	getter := &WebhookStore{
		DB: db,
	}

	return getter, getter.Health(ctx)
}

type WebhookStore struct {
	DB *sqlx.DB
}

const (
	ListQuery string = `
select
  wh.id as id,
  whc.id as webhook_config_id,
  wh.url as url,
  wh.secret as secret
from streaming.webhooks wh 
inner join streaming.webhook_configs whc
  on whc.id=wh.webhook_config_id 
inner join streaming.users u
  on u.id = whc.owner_id
where u.twitch_id = $1;
`
	GetWebhookConfigIDQuery string = `
select
  whc.id
from streaming.webhook_configs whc
inner join streaming.users u
  on whc.owner_id = u.id
where u.twitch_id = $1;
`
	CreateQuery string = `
insert into streaming.webhooks
  (webhook_config_id, url, secret)
VALUES
  ($1, $2, $3)
returning id;`
	ReadQuery string = `
select
  id,
  webhook_config_id,
  url,
  secret
from streaming.webhooks
where id = $1;
`
	UpdateQuery string = `
update streaming.webhooks
set
  id = $2,
  url = $3,
  secret = $4,
where id = $1;
`
	DeleteQuery string = `
delete
from streaming.webhooks
where id = $1;
`
)

func mapWebhooks(webhooksIn []Webhook) []webhooks.Webhook {
	webhooks := make([]webhooks.Webhook, 0, len(webhooksIn))
	for _, webhook := range webhooksIn {
		webhooks = append(webhooks, webhook)
	}
	return webhooks
}

func (whs *WebhookStore) List(ctx context.Context, twitch_id string) ([]webhooks.Webhook, error) {
	stmt, err := whs.DB.PreparexContext(ctx, ListQuery)
	if err != nil {
		return nil, fmt.Errorf("failed to prepare statement: %w", err)
	}

	webhooks := []Webhook{}
	err = stmt.SelectContext(ctx, &webhooks, twitch_id)
	if err != nil {
		return nil, fmt.Errorf("Failed to query webhooks for %s: %w", twitch_id, err)
	}

	return mapWebhooks(webhooks), nil
}

func (whs *WebhookStore) Create(ctx context.Context, twitch_id string, wh webhooks.Webhook) (string, error) {
	tx, err := whs.DB.Beginx()
	if err != nil {
		tx.Rollback()
		return "", err
	}

	stmt, err := whs.DB.PreparexContext(ctx, GetWebhookConfigIDQuery)
	if err != nil {
		tx.Rollback()
		return "", fmt.Errorf("failed to prepare statement: %w", err)
	}

	var wh_config_id uuid.UUID
	err = stmt.GetContext(ctx, &wh_config_id, twitch_id)

	stmt, err = whs.DB.PreparexContext(ctx, CreateQuery)
	if err != nil {
		tx.Rollback()
		return "", fmt.Errorf("failed to prepare statement: %w", err)
	}

	var id uuid.UUID
	err = stmt.GetContext(ctx, &id, wh_config_id, wh.GetURL(), wh.GetSecret())
	if err != nil {
		tx.Rollback()
		return "", fmt.Errorf("failed to create new webhook: %w", err)
	}

	err = tx.Commit()
	if err != nil {
		tx.Rollback()
		return "", err
	}
	return id.String(), nil
}

func (whs *WebhookStore) Read(ctx context.Context, id string) (webhooks.Webhook, error) {
	uid, err := uuid.Parse(id)
	if err != nil {
		return nil, fmt.Errorf("failed to parse ID (%s) to get: %w", uid, err)
	}

	stmt, err := whs.DB.PreparexContext(ctx, ReadQuery)
	if err != nil {
		return nil, fmt.Errorf("failed to prepare statement: %w", err)
	}

	var webhook Webhook
	err = stmt.GetContext(ctx, &webhook, uid)
	if err != nil {
		return nil, fmt.Errorf("failed to get response: %w", err)
	}

	return webhook, nil
}

func (whs *WebhookStore) Update(ctx context.Context, webhook webhooks.Webhook) error {
	wh, ok := webhook.(*Webhook)
	if !ok {
		return fmt.Errorf("when writing to the database you need to pass a database.Webhook implementation")
	}
	uid, err := uuid.Parse(webhook.GetID())
	if err != nil {
		return fmt.Errorf("failed to parse ID (%s) to get: %w", uid, err)
	}

	stmt, err := whs.DB.PreparexContext(ctx, UpdateQuery)
	if err != nil {
		return fmt.Errorf("failed to prepare statement: %w", err)
	}

	result, err := stmt.ExecContext(
		ctx,
		uid,
		wh.URL,
		wh.Secret,
	)
	if err != nil {
		return fmt.Errorf("failed to get result: %w", err)
	}

	num, err := result.RowsAffected()
	if err != nil {
		return fmt.Errorf("failed to get number of rows affected: %w", err)
	}

	if num != 1 {
		return fmt.Errorf("unexpected number of rows affected: %d", num)
	}

	return nil
}

func (whs *WebhookStore) Delete(ctx context.Context, id string) error {
	uid, err := uuid.Parse(id)
	if err != nil {
		return fmt.Errorf("failed to parse ID (%s) to get: %w", uid, err)
	}

	stmt, err := whs.DB.PreparexContext(ctx, DeleteQuery)
	if err != nil {
		return fmt.Errorf("failed to prepare statement: %w", err)
	}

	_, err = stmt.ExecContext(ctx, uid)
	if err != nil {
		return fmt.Errorf("failed to get response: %w", err)
	}

	return nil
}

func (whs *WebhookStore) Health(ctx context.Context) error {
	return whs.DB.PingContext(ctx)
}
