package database

import (
	"context"
	"fmt"

	"github.com/google/uuid"
	"github.com/jmoiron/sqlx"
	"gitlab.com/bancast/streamtools/twitch-tools/pkg/auth"
)

var _ auth.TokenStore = &DatabaseAuthStore{}

const (
	GetQuery string = `
select
  t.id, t.created_at, t.updated_at, t.access_token, t.refresh_token
from streaming.tokens t
inner join streaming.users u on t.user_id = u.id
where u.twitch_id = $1;
`
	GetUserIDQuery string = `
select id
from streaming.users
where twitch_id = $1;
`
	StoreQuery string = `
insert into streaming.tokens
  (user_id, access_token, refresh_token)
VALUES
  ($1, $2, $3)
on conflict (user_id) do update
set
  access_token = $2,
  refresh_token = $3;
`
)

type DatabaseAuthStore struct {
	DB *sqlx.DB
}

func NewAuthStore(_ context.Context, db *sqlx.DB) *DatabaseAuthStore {
	return &DatabaseAuthStore{
		DB: db,
	}
}

func (store *DatabaseAuthStore) Get(ctx context.Context, twitch_id string) (*auth.Tokens, error) {
	stmt, err := store.DB.PreparexContext(ctx, GetQuery)
	if err != nil {
		return nil, fmt.Errorf("failed to prepare statement: %w", err)
	}

	var token Token
	err = stmt.GetContext(ctx, &token, twitch_id)
	if err != nil {
		return nil, fmt.Errorf("failed to get token: %w", err)
	}

	return &auth.Tokens{
		AccessToken:  token.AccessToken,
		RefreshToken: token.RefreshToken,
	}, nil
}

func (store *DatabaseAuthStore) Store(ctx context.Context, twitch_id string, tokens auth.Tokens) error {
	tx, err := store.DB.Beginx()
	if err != nil {
		return fmt.Errorf("failed to being transaction: %w", err)
	}
	stmt, err := store.DB.PreparexContext(ctx, GetUserIDQuery)
	if err != nil {
		tx.Rollback()
		return err
	}

	var userID uuid.UUID
	err = stmt.GetContext(ctx, &userID, twitch_id)
	if err != nil {
		tx.Rollback()
		return err
	}

	stmt, err = store.DB.PreparexContext(ctx, StoreQuery)
	if err != nil {
		tx.Rollback()
		return fmt.Errorf("failed to prepare for insert of token: %w", err)
	}

	_, err = stmt.ExecContext(ctx, userID, tokens.AccessToken, tokens.RefreshToken)
	if err != nil {
		tx.Rollback()
		return fmt.Errorf("failed to store token: %w", err)
	}

	err = tx.Commit()
	if err != nil {
		tx.Rollback()
		return fmt.Errorf("failed to commit changes: %w", err)
	}
	return nil
}
