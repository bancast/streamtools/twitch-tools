package webhooks

import (
	"context"
	"fmt"
	"time"
)

var _ Store = &CachedStore{}

func NewCachedStore(i Store, ttl time.Duration) *CachedStore {
	return &CachedStore{
		Store: i,
		TTL:   ttl,
		Cache: make(map[string]CacheEntry),
	}
}

type CacheEntry struct {
	Time     time.Time
	Webhooks []Webhook
}

type CachedStore struct {
	Store
	TTL   time.Duration
	Cache map[string]CacheEntry
}

func (s *CachedStore) List(ctx context.Context, twitch_id string) ([]Webhook, error) {

	if cacheEntry, ok := s.Cache[twitch_id]; ok && time.Since(cacheEntry.Time) < s.TTL {
		return cacheEntry.Webhooks, nil
	}

	webhooks, err := s.Store.List(ctx, twitch_id)
	if err != nil {
		return nil, fmt.Errorf("failed to get responses: %w", err)
	}

	s.Cache[twitch_id] = CacheEntry{
		Time:     time.Now(),
		Webhooks: webhooks,
	}

	return webhooks, nil
}
