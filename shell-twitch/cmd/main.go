package cmd

import (
	"fmt"
	"os"

	"github.com/spf13/cobra"
	"gitlab.com/bancast/streamtools/twitch-tools/pkg/logging"
	"gitlab.com/bancast/streamtools/twitch-tools/shell-twitch/cmd/listen"
)

func NewRootCmd() *cobra.Command {
	cmd := &cobra.Command{
		Use:   "shell-twitch",
		Short: "React to given message patterns and react by running a command",
	}

	logger := logging.SetupLoggerFlags(cmd.PersistentFlags())

	cmd.AddCommand(listen.NewListenCommand(logger))

	return cmd
}

func Execute() {
	rootCmd := NewRootCmd()

	if err := rootCmd.Execute(); err != nil {
		fmt.Fprintln(os.Stderr, err)
		os.Exit(1)
	}
}
