package listen

import (
	"context"
	"fmt"

	"github.com/spf13/cobra"
	gateway "gitlab.com/bancast/streamtools/twitch-tools/pkg/grpc"
	"gitlab.com/bancast/streamtools/twitch-tools/pkg/instrumentation"
	"gitlab.com/bancast/streamtools/twitch-tools/pkg/logging"
	"gitlab.com/bancast/streamtools/twitch-tools/pkg/pubsub"
	"gitlab.com/bancast/streamtools/twitch-tools/protos/twitch/chat"
	"gitlab.com/bancast/streamtools/twitch-tools/protos/twitch/messages"
)

// NewListenCommand creates a cobra command given a logger.
//
// This will listen for messages on the queue and push them to
// webhookd to be sent to clients.
func NewListenCommand(logger *logging.Logger) *cobra.Command {
	cmd := &cobra.Command{
		Use:   "listen",
		Short: "Listen for new messages to react to",
	}

	pubsubFlags := pubsub.SetupFlags(cmd.PersistentFlags())
	chatTwitchFlags := gateway.SetupClientFlags("chat-twitch", cmd.PersistentFlags())
	pattern := cmd.PersistentFlags().String("pattern", ".*", "Regex pattern to match for message return")
	channel := cmd.PersistentFlags().String("channel", "jackatbancast", "Channel to listen to")

	instrumentationFlags := instrumentation.SetupFlags(cmd.PersistentFlags())

	cmd.RunE = func(cmd *cobra.Command, args []string) error {
		logger := logger.Logr()

		ctx, cancel := context.WithCancel(cmd.Context())
		defer cancel()

		go instrumentationFlags.RunMetricsServer(ctx, logger.WithName("metrics"))
		RegisterDefaultViews()

		ch, closeCh := pubsubFlags.MustChannel(ctx, logger)
		defer closeCh()

		chatTwitchGRPCClient := chatTwitchFlags.MustClient(ctx, logger)
		defer chatTwitchGRPCClient.Close()
		logger.Info("creating client", "server", "twitch-chat")
		twitchChat := chat.NewTwitchChatClient(chatTwitchGRPCClient)

		msgs := make(chan *messages.TwitchMessage)

		logger.Info("creating a new queue")
		newQueue, err := pubsub.NewTemporaryQueue(ctx, ch)
		if err != nil {
			return fmt.Errorf("failed to create new queue: %w", err)
		}
		logger.Info("created new queue", "queue", newQueue)

		logger.Info("starting message read loop")
		go pubsub.ReadMessages(ctx, ch, newQueue, msgs)

		logger.Info("starting message logging")
		return ReadMessages(ctx, logger, twitchChat, *channel, *pattern, args, msgs)
	}

	return cmd
}
