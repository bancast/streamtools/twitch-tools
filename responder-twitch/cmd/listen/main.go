package listen

import (
	"context"
	"errors"
	"fmt"
	"time"

	"github.com/go-logr/logr"
	"github.com/spf13/cobra"
	"gitlab.com/bancast/streamtools/twitch-tools/pkg/database"
	dbResponses "gitlab.com/bancast/streamtools/twitch-tools/pkg/database/responses"
	gateway "gitlab.com/bancast/streamtools/twitch-tools/pkg/grpc"
	"gitlab.com/bancast/streamtools/twitch-tools/pkg/instrumentation"
	"gitlab.com/bancast/streamtools/twitch-tools/pkg/logging"
	"gitlab.com/bancast/streamtools/twitch-tools/pkg/pubsub"
	"gitlab.com/bancast/streamtools/twitch-tools/pkg/responses"
	"gitlab.com/bancast/streamtools/twitch-tools/protos/twitch/chat"
	"gitlab.com/bancast/streamtools/twitch-tools/protos/twitch/messages"
	twitchResponses "gitlab.com/bancast/streamtools/twitch-tools/protos/twitch/responses"
	"google.golang.org/grpc"

	_ "go.opencensus.io/stats"
	_ "go.opencensus.io/stats/view"
	_ "go.opencensus.io/tag"
)

const (
	QueueName string = "responder"
)

func NewListenCommand(logger *logging.Logger) *cobra.Command {
	cmd := &cobra.Command{
		Use:   "listen",
		Short: "Listen for new messages",
	}

	dbFlags := database.SetupFlags(cmd.PersistentFlags())
	pubsubFlags := pubsub.SetupFlags(cmd.PersistentFlags())
	chatTwitchFlags := gateway.SetupClientFlags("chat-twitch", cmd.PersistentFlags())
	grpcServerFlags := gateway.SetupServerFlags(cmd.PersistentFlags())
	instrumentationFlags := instrumentation.SetupFlags(cmd.PersistentFlags())

	cmd.RunE = func(cmd *cobra.Command, args []string) error {
		logger := logger.Logr()

		ctx, cancel := context.WithCancel(cmd.Context())
		defer cancel()

		go instrumentationFlags.RunMetricsServer(ctx, logger.WithName("metrics"))

		db := dbFlags.MustDatabase(ctx, logger)
		defer db.Close()
		ch, closeCh := pubsubFlags.MustChannel(ctx, logger)
		defer closeCh()

		logger.V(3).Info("creating store adaptor")
		store, err := dbResponses.NewDatabaseResponsesStore(ctx, db)
		if err != nil {
			return fmt.Errorf("failed to create database getter: %w", err)
		}

		logger.Info("starting configuration api")
		go ConfigurationAPI(ctx, logger.WithName("api-config"), grpcServerFlags, store)

		logger.Info("creating responder")
		responder, err := NewResponder(ctx, logger.WithName("responder"), store)
		if err != nil {
			return fmt.Errorf("failed to create responder: %w", err)
		}

		chatTwitchGRPCClient := chatTwitchFlags.MustClient(ctx, logger)
		defer chatTwitchGRPCClient.Close()
		logger.Info("creating client", "server", "twitch-chat")
		twitchChat := chat.NewTwitchChatClient(chatTwitchGRPCClient)

		msgChan := make(chan *messages.TwitchMessage)
		logger.Info("starting responder")
		go respond(ctx, logger.WithName("response-loop"), responder, twitchChat, msgChan)

		logger.Info("starting message ingestion")
		return pubsub.ReadTwitchMessages(ctx, ch, QueueName, msgChan)
	}

	return cmd
}

func ConfigurationAPI(ctx context.Context, logger logr.Logger, serverFlags *gateway.ServerFlags, store responses.Store) error {
	server := ResponsesConfigurationServer{logger: logger, store: store}
	err := serverFlags.RegisterGateway(
		ctx, logger,
		func(ctx context.Context, grpcServer *grpc.Server) error {
			twitchResponses.RegisterResponsesConfigurationServer(grpcServer, server)
			return nil
		},
		twitchResponses.RegisterResponsesConfigurationHandlerFromEndpoint,
	)
	if err != nil {
		return fmt.Errorf("failed to run with gateway: %w", err)
	}

	return serverFlags.Listen(ctx, logger.WithName("server"))
}

func respond(ctx context.Context, logger logr.Logger, responder *responses.Responder, twitchChat chat.TwitchChatClient, msgChan <-chan *messages.TwitchMessage) error {
	logger.V(3).Info("reading messages from the channel")
	for msg := range msgChan {
		logger := logger.WithValues("twitch_id", msg.TwitchId)
		logger.Info("read message")

		baseMessage := msg.GetMsg()
		if baseMessage == nil {
			logger.Info("no message in wrapper")
			continue
		}

		privMsg, ok := baseMessage.(*messages.TwitchMessage_PrivateMessage)
		if !ok {
			logger.Info("not a private message, skipping")
			continue
		}

		logger = logger.WithValues(
			"channel", privMsg.PrivateMessage.Channel,
			"msg", privMsg.PrivateMessage.Msg,
		)

		logger.Info("responding to message")
		resp, err := responder.Respond(ctx, msg.TwitchId, privMsg.PrivateMessage)
		if err != nil && !errors.Is(err, responses.ErrNoResponseMatch) {
			logger.Error(err, "failed to test response")
			continue
		}

		if resp != "" {
			logger.Info("response created")
			ctx, cancel := context.WithTimeout(ctx, 5*time.Second)

			logger.Info("sending response to chat-twitch")
			_, err = twitchChat.Send(ctx, &chat.TwitchChatRequest{
				TwitchId: msg.TwitchId,
				Body:     resp,
			})
			if err != nil {
				logger.Error(err, "failed to send response to chat-twitch")
				cancel()
				continue
			}

			logger.Info("sent response")
			cancel()
		}
	}

	return nil
}

func NewResponder(ctx context.Context, logger logr.Logger, store responses.Store) (*responses.Responder, error) {
	logger.Info("creating responder server")
	responder := responses.NewResponder(
		responses.NewCachedStore(store, 15*time.Second),
	)
	if responder == nil {
		logger.Info("created nil responder?")
		return nil, fmt.Errorf("nil responder created?")
	}

	logger.Info("responder created")
	return responder, nil
}
