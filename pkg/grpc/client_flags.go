package grpc

import (
	"context"
	"crypto/tls"
	"fmt"

	"github.com/go-logr/logr"
	"github.com/spf13/pflag"
	"go.opencensus.io/plugin/ocgrpc"
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials"
)

type ClientFlags struct {
	name     string
	insecure *bool
	uri      *string
}

func (f ClientFlags) Client(ctx context.Context, logger logr.Logger) (*grpc.ClientConn, error) {
	logger.Info("connecting to server", "server", f.name, "uri", *f.uri)
	opts := []grpc.DialOption{grpc.WithStatsHandler(&ocgrpc.ClientHandler{})}

	if *f.insecure {
		opts = append(opts, grpc.WithInsecure())
	} else {
		opts = append(opts, grpc.WithTransportCredentials(credentials.NewTLS(&tls.Config{InsecureSkipVerify: true})))
	}

	client, err := grpc.Dial(*f.uri, opts...)
	if err != nil {
		return nil, fmt.Errorf("failed to connect to %s server: %w", f.name, err)
	}

	return client, nil
}

func (f ClientFlags) MustClient(ctx context.Context, logger logr.Logger) *grpc.ClientConn {
	client, err := f.Client(ctx, logger)
	if err != nil {
		panic(err)
	}

	return client
}

func SetupClientFlags(clientName string, flags *pflag.FlagSet) *ClientFlags {
	return &ClientFlags{
		name: clientName,
		uri: flags.String(
			fmt.Sprintf("%s-uri", clientName),
			fmt.Sprintf("localhost:%d", DefaultGrpcPort),
			fmt.Sprintf("URI to connect to GRPC port for %s", clientName),
		),
		insecure: flags.Bool(
			fmt.Sprintf("%s-insecure", clientName),
			false,
			"Mark the grpc connection as insecure",
		),
	}
}
