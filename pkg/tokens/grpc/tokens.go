package grpc

import (
	"context"
	"fmt"

	"gitlab.com/bancast/streamtools/twitch-tools/pkg/tokens"
	"gitlab.com/bancast/streamtools/twitch-tools/protos/identity"
	"google.golang.org/grpc"
)

var _ tokens.Controller = &Client{}

func NewClient(conn grpc.ClientConnInterface) *Client {
	return &Client{
		TokenProvisionerClient: identity.NewTokenProvisionerClient(conn),
		TokenManagementClient:  identity.NewTokenManagementClient(conn),
	}
}

type Client struct {
	identity.TokenProvisionerClient
	identity.TokenManagementClient
}

func (c *Client) Create(ctx context.Context, options *tokens.CreateOptions) (string, error) {
	ctx, cancel := context.WithCancel(ctx)
	defer cancel()

	resp, err := c.TokenProvisionerClient.Create(ctx, &identity.TokenCreateRequest{
		User:     options.User,
		Nonce:    options.Nonce,
		Name:     options.Name,
		Lifetime: int64(options.Lifetime),
		Scopes:   options.Scopes,
		Comment:  options.Comment,
	})
	if err != nil {
		return "", fmt.Errorf("failed to create token: %w", err)
	}

	return resp.Id, nil
}

func (c *Client) Exchange(ctx context.Context, id string, nonce string) (string, error) {
	ctx, cancel := context.WithCancel(ctx)
	defer cancel()

	resp, err := c.TokenProvisionerClient.Exchange(ctx, &identity.TokenExchangeRequest{
		Id:    id,
		Nonce: nonce,
	})
	if err != nil {
		return "", fmt.Errorf("failed to create token: %w", err)
	}

	return resp.Token, nil
}

func (c *Client) List(ctx context.Context) ([]tokens.Token, error) {
	// TODO: pass through our token
	ctx, cancel := context.WithCancel(ctx)
	defer cancel()

	resp, err := c.TokenManagementClient.List(ctx, &identity.TokenListRequest{})
	if err != nil {
		return nil, fmt.Errorf("failed to create token: %w", err)
	}

	return mapTokens(resp.Tokens), nil
}

func mapTokens(protoTokens []*identity.TokenInfo) []tokens.Token {
	tokens := make([]tokens.Token, 0, len(protoTokens))

	for _, t := range protoTokens {
		tokens = append(tokens, convertTokenInfo(t))
	}

	return tokens
}

func convertTokenInfo(token *identity.TokenInfo) tokens.Token {
	// TODO: add implementation
	return nil
}

func (c *Client) Read(ctx context.Context, id string) (tokens.Token, error) {
	// TODO: pass through our token
	ctx, cancel := context.WithCancel(ctx)
	defer cancel()

	resp, err := c.TokenManagementClient.Read(ctx, &identity.TokenReadRequest{
		Id: id,
	})
	if err != nil {
		return nil, fmt.Errorf("failed to create token: %w", err)
	}

	return convertTokenInfo(resp.Token), nil
}

func (c *Client) Refresh(ctx context.Context) (string, error) {
	// TODO: pass through our token
	ctx, cancel := context.WithCancel(ctx)
	defer cancel()

	resp, err := c.TokenManagementClient.Refresh(ctx, &identity.TokenRefreshRequest{})
	if err != nil {
		return "", fmt.Errorf("failed to create token: %w", err)
	}

	return resp.Token, nil
}
