package listen

import (
	"context"

	"github.com/go-logr/logr"
	"google.golang.org/grpc"
	"google.golang.org/grpc/codes"
	"google.golang.org/protobuf/types/known/timestamppb"

	dbUsers "gitlab.com/bancast/streamtools/twitch-tools/pkg/database/users"
	"gitlab.com/bancast/streamtools/twitch-tools/pkg/tokens"
	"gitlab.com/bancast/streamtools/twitch-tools/pkg/users"
	"gitlab.com/bancast/streamtools/twitch-tools/protos/twitch/identity"
)

type UserManagementServer struct {
	identity.UnimplementedUserManagementServer
	store  users.Store
	logger logr.Logger
}

func (u UserManagementServer) Self(ctx context.Context, req *identity.SelfRequest) (*identity.User, error) {
	ctxProvisioner := tokens.ContextProvisioner{}
	token, err := ctxProvisioner.GetToken(ctx)
	if err != nil {
		u.logger.Error(err, "failed to get token")
		return nil, grpc.Errorf(codes.PermissionDenied, "failed to get self")
	}

	user, err := u.store.Read(ctx, token.GetUserID())
	if err != nil {
		u.logger.Error(
			err, "failed to lookup user",
			"user", token.GetUserID,
		)
		return nil, grpc.Errorf(codes.Internal, "failed to get user")
	}

	return convertUser(user), nil
}

func (u UserManagementServer) Create(ctx context.Context, req *identity.CreateRequest) (*identity.User, error) {
	user, err := u.store.Create(ctx, dbUsers.NewUser("", &req.TwitchId, req.Username, req.Bot))
	if err != nil {
		u.logger.Error(err, "failed to create user", "username", req.Username)
		return nil, grpc.Errorf(codes.Internal, "failed to create user")
	}

	return convertUser(user), nil
}

func (u UserManagementServer) Read(ctx context.Context, req *identity.ReadRequest) (*identity.User, error) {
	user, err := u.store.Read(ctx, req.Id)
	if err != nil {
		return nil, grpc.Errorf(codes.NotFound, "user %s not found", req.Id)
	}

	return convertUser(user), nil
}

func (u UserManagementServer) Lookup(ctx context.Context, req *identity.LookupRequest) (*identity.User, error) {
	user, err := u.store.Lookup(ctx, req.Username)
	if err != nil {
		u.logger.Error(err, "failed to lookup user", "username", req.Username)
		return nil, grpc.Errorf(codes.NotFound, "user %s not found", req.Username)
	}

	return convertUser(user), nil
}

func (u UserManagementServer) List(ctx context.Context, req *identity.ListRequest) (*identity.ListResponse, error) {
	users, err := u.store.List(ctx)
	if err != nil {
		return nil, grpc.Errorf(codes.Internal, "failed to list users")
	}

	return &identity.ListResponse{Users: mapUsers(users)}, nil
}

func mapUsers(users []users.User) []*identity.User {
	protoUsers := make([]*identity.User, 0, len(users))

	for _, user := range users {
		protoUsers = append(protoUsers, convertUser(user))
	}

	return protoUsers
}

func convertUser(u users.User) *identity.User {
	return &identity.User{
		Id:        u.GetID(),
		CreatedAt: timestamppb.New(u.GetCreatedAt().UTC()),
		UpdatedAt: timestamppb.New(u.GetUpdatedAt().UTC()),
		Username:  u.GetUsername(),
		Bot:       u.GetBot(),
		TwitchId:  u.GetTwitchID(),
	}
}

func (u UserManagementServer) Update(ctx context.Context, req *identity.UpdateRequest) (*identity.UpdateResponse, error) {
	err := u.store.Update(ctx, dbUsers.NewUser(req.User.Id, &req.User.TwitchId, req.User.Username, req.User.Bot))

	if err != nil {
		return nil, grpc.Errorf(codes.Internal, "failed to update user %s", req.User.Id)
	}

	return &identity.UpdateResponse{}, nil
}

func (u UserManagementServer) Delete(ctx context.Context, req *identity.DeleteRequest) (*identity.DeleteResponse, error) {
	err := u.store.Delete(ctx, req.Id)
	if err != nil {
		return nil, grpc.Errorf(codes.Internal, "failed to delete user %s", req.Id)
	}
	return &identity.DeleteResponse{}, nil
}
