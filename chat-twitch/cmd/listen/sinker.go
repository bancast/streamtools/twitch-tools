package listen

import (
	"context"

	twitchIRC "github.com/gempir/go-twitch-irc/v2"
	"gitlab.com/bancast/streamtools/twitch-tools/protos/twitch/messages"
	"gitlab.com/bancast/streamtools/twitch-tools/protos/twitch/sink"
	"google.golang.org/grpc"
	"google.golang.org/protobuf/types/known/timestamppb"
)

type TwitchIDKey struct{}

func SetTwitchIDContext(ctx context.Context, twitch_id string) context.Context {
	return context.WithValue(ctx, TwitchIDKey{}, twitch_id)
}

func GetTwitchIDContext(ctx context.Context) string {
	return ctx.Value(TwitchIDKey{}).(string)
}

type TwitchSinker struct {
	Client sink.TwitchMessageSinkClient
}

func NewTwitchSinker(client grpc.ClientConnInterface) *TwitchSinker {
	return &TwitchSinker{
		Client: sink.NewTwitchMessageSinkClient(client),
	}
}

func (t *TwitchSinker) MapUser(user twitchIRC.User) *messages.User {
	return &messages.User{
		Id:          user.ID,
		DisplayName: user.DisplayName,
		Name:        user.Name,
		Color:       user.Color,
		Badges:      t.MapBadges(user.Badges),
	}
}

func (t *TwitchSinker) MapBadges(badges map[string]int) map[string]int64 {
	b := map[string]int64{}
	for key, value := range badges {
		b[key] = int64(value)
	}
	return b
}

func (t *TwitchSinker) MapEmotes(emotes []*twitchIRC.Emote) []*messages.Emote {
	e := make([]*messages.Emote, 0, len(emotes))
	for _, emote := range emotes {
		e = append(e, &messages.Emote{
			Name:  emote.Name,
			Id:    emote.ID,
			Count: int64(emote.Count),
		})
	}
	return e
}

func (t *TwitchSinker) NewWhisperMessageHandler(ctx context.Context, f func(twitchIRC.WhisperMessage)) func(twitchIRC.WhisperMessage) {
	return func(msg twitchIRC.WhisperMessage) {
		f(msg)
		t.Client.Sink(
			ctx,
			&messages.TwitchMessage{
				TwitchId: GetTwitchIDContext(ctx),
				Msg: &messages.TwitchMessage_WhisperMessage{
					&messages.WhisperMessage{
						User:     t.MapUser(msg.User),
						Raw:      msg.Raw,
						Type:     messages.MessageType(msg.Type),
						RawType:  msg.RawType,
						Tags:     msg.Tags,
						Msg:      msg.Message,
						Target:   msg.Target,
						ThreadId: msg.ThreadID,
						Emote:    t.MapEmotes(msg.Emotes),
						Action:   msg.Action,
					},
				},
			},
		)
	}
}

func (t *TwitchSinker) NewPrivateMessageHandler(ctx context.Context, f func(twitchIRC.PrivateMessage)) func(twitchIRC.PrivateMessage) {
	return func(msg twitchIRC.PrivateMessage) {
		f(msg)
		t.Client.Sink(
			ctx,
			&messages.TwitchMessage{
				TwitchId: GetTwitchIDContext(ctx),
				Msg: &messages.TwitchMessage_PrivateMessage{
					&messages.PrivateMessage{
						User:    t.MapUser(msg.User),
						Raw:     msg.Raw,
						Type:    messages.MessageType(msg.Type),
						RawType: msg.RawType,
						Tags:    msg.Tags,
						Msg:     msg.Message,
						Channel: msg.Channel,
						RoomId:  msg.RoomID,
						Id:      msg.ID,
						Time:    timestamppb.New(msg.Time),
						Emote:   t.MapEmotes(msg.Emotes),
						Bits:    int64(msg.Bits),
						Action:  msg.Action,
					},
				},
			},
		)
	}
}

func (t *TwitchSinker) NewClearChatMessageHandler(ctx context.Context, f func(twitchIRC.ClearChatMessage)) func(twitchIRC.ClearChatMessage) {
	return func(msg twitchIRC.ClearChatMessage) {
		f(msg)
		t.Client.Sink(
			ctx,
			&messages.TwitchMessage{
				TwitchId: GetTwitchIDContext(ctx),
				Msg: &messages.TwitchMessage_ClearChatMessage{
					&messages.ClearChatMessage{
						Raw:            msg.Raw,
						Type:           messages.MessageType(msg.Type),
						RawType:        msg.RawType,
						Tags:           msg.Tags,
						Msg:            msg.Message,
						Channel:        msg.Channel,
						RoomId:         msg.RoomID,
						Time:           timestamppb.New(msg.Time),
						TargetUserId:   msg.TargetUserID,
						TargetUserName: msg.TargetUsername,
					},
				},
			},
		)
	}
}

func (t *TwitchSinker) NewClearMessageHandler(ctx context.Context, f func(twitchIRC.ClearMessage)) func(twitchIRC.ClearMessage) {
	return func(msg twitchIRC.ClearMessage) {
		f(msg)
		t.Client.Sink(
			ctx,
			&messages.TwitchMessage{
				TwitchId: GetTwitchIDContext(ctx),
				Msg: &messages.TwitchMessage_ClearMessage{
					&messages.ClearMessage{
						Raw:         msg.Raw,
						Type:        messages.MessageType(msg.Type),
						RawType:     msg.RawType,
						Tags:        msg.Tags,
						Msg:         msg.Message,
						Channel:     msg.Channel,
						Login:       msg.Login,
						TargetMsgId: msg.TargetMsgID,
					},
				},
			},
		)
	}
}

func (t *TwitchSinker) NewRoomStateMessageHandler(ctx context.Context, f func(twitchIRC.RoomStateMessage)) func(twitchIRC.RoomStateMessage) {
	return func(msg twitchIRC.RoomStateMessage) {
		f(msg)
		t.Client.Sink(
			ctx,
			&messages.TwitchMessage{
				TwitchId: GetTwitchIDContext(ctx),
				Msg: &messages.TwitchMessage_RoomStateMessage{
					&messages.RoomStateMessage{
						Raw:     msg.Raw,
						Type:    messages.MessageType(msg.Type),
						RawType: msg.RawType,
						Tags:    msg.Tags,
						Msg:     msg.Message,
						Channel: msg.Channel,
						RoomId:  msg.RoomID,
						State:   t.MapBadges(msg.State),
					},
				},
			},
		)
	}
}

func (t *TwitchSinker) NewUserNoticeMessageHandler(ctx context.Context, f func(twitchIRC.UserNoticeMessage)) func(twitchIRC.UserNoticeMessage) {
	return func(msg twitchIRC.UserNoticeMessage) {
		f(msg)
		t.Client.Sink(
			ctx,
			&messages.TwitchMessage{
				TwitchId: GetTwitchIDContext(ctx),
				Msg: &messages.TwitchMessage_UserNoticeMessage{
					&messages.UserNoticeMessage{
						User:      t.MapUser(msg.User),
						Raw:       msg.Raw,
						Type:      messages.MessageType(msg.Type),
						RawType:   msg.RawType,
						Tags:      msg.Tags,
						Msg:       msg.Message,
						Channel:   msg.Channel,
						RoomId:    msg.RoomID,
						Id:        msg.ID,
						Time:      timestamppb.New(msg.Time),
						Emotes:    t.MapEmotes(msg.Emotes),
						MsgId:     msg.MsgID,
						MsgParams: msg.MsgParams,
						SystemMsg: msg.SystemMsg,
					},
				},
			},
		)
	}
}

func (t *TwitchSinker) NewUserStateMessageHandler(ctx context.Context, f func(twitchIRC.UserStateMessage)) func(twitchIRC.UserStateMessage) {
	return func(msg twitchIRC.UserStateMessage) {
		f(msg)
		t.Client.Sink(
			ctx,
			&messages.TwitchMessage{
				TwitchId: GetTwitchIDContext(ctx),
				Msg: &messages.TwitchMessage_UserStateMessage{
					&messages.UserStateMessage{
						User:      t.MapUser(msg.User),
						Raw:       msg.Raw,
						Type:      messages.MessageType(msg.Type),
						RawType:   msg.RawType,
						Tags:      msg.Tags,
						Msg:       msg.Message,
						Channel:   msg.Channel,
						EmoteSets: msg.EmoteSets,
					},
				},
			},
		)
	}
}

func (t *TwitchSinker) NewGlobalUserStateMessageHandler(ctx context.Context, f func(twitchIRC.GlobalUserStateMessage)) func(twitchIRC.GlobalUserStateMessage) {
	return func(msg twitchIRC.GlobalUserStateMessage) {
		f(msg)
		t.Client.Sink(
			ctx,
			&messages.TwitchMessage{
				TwitchId: GetTwitchIDContext(ctx),
				Msg: &messages.TwitchMessage_GlobalUserStateMessage{
					&messages.GlobalUserStateMessage{
						User:      t.MapUser(msg.User),
						Raw:       msg.Raw,
						Type:      messages.MessageType(msg.Type),
						RawType:   msg.RawType,
						Tags:      msg.Tags,
						EmoteSets: msg.EmoteSets,
					},
				},
			},
		)
	}
}

func (t *TwitchSinker) NewNoticeMessageHandler(ctx context.Context, f func(twitchIRC.NoticeMessage)) func(twitchIRC.NoticeMessage) {
	return func(msg twitchIRC.NoticeMessage) {
		f(msg)
		t.Client.Sink(
			ctx,
			&messages.TwitchMessage{
				TwitchId: GetTwitchIDContext(ctx),
				Msg: &messages.TwitchMessage_NoticeMessage{
					&messages.NoticeMessage{
						Raw:     msg.Raw,
						Type:    messages.MessageType(msg.Type),
						RawType: msg.RawType,
						Tags:    msg.Tags,
						Msg:     msg.Message,
						Channel: msg.Channel,
						MsgId:   msg.MsgID,
					},
				},
			},
		)
	}
}

func (t *TwitchSinker) NewUserJoinMessageHandler(ctx context.Context, f func(twitchIRC.UserJoinMessage)) func(twitchIRC.UserJoinMessage) {
	return func(msg twitchIRC.UserJoinMessage) {
		f(msg)
		t.Client.Sink(
			ctx,
			&messages.TwitchMessage{
				TwitchId: GetTwitchIDContext(ctx),
				Msg: &messages.TwitchMessage_UserJoinMessage{
					&messages.UserJoinMessage{
						Channel: msg.Channel,
						User:    msg.User,
					},
				},
			},
		)
	}
}

func (t *TwitchSinker) NewUserPartMessageHandler(ctx context.Context, f func(twitchIRC.UserPartMessage)) func(twitchIRC.UserPartMessage) {
	return func(msg twitchIRC.UserPartMessage) {
		f(msg)
		t.Client.Sink(
			ctx,
			&messages.TwitchMessage{
				TwitchId: GetTwitchIDContext(ctx),
				Msg: &messages.TwitchMessage_UserPartMessage{
					&messages.UserPartMessage{
						Channel: msg.Channel,
						User:    msg.User,
					},
				},
			},
		)
	}
}

func (t *TwitchSinker) NewReconnectMessageHandler(ctx context.Context, f func(twitchIRC.ReconnectMessage)) func(twitchIRC.ReconnectMessage) {
	return func(msg twitchIRC.ReconnectMessage) {
		f(msg)
		t.Client.Sink(
			ctx,
			&messages.TwitchMessage{
				TwitchId: GetTwitchIDContext(ctx),
				Msg: &messages.TwitchMessage_ReconnectMessage{
					&messages.ReconnectMessage{
						Raw:     msg.Raw,
						Type:    messages.MessageType(msg.Type),
						RawType: msg.RawType,
					},
				},
			},
		)
	}
}
