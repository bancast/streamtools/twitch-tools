#+title: Twitch Tools
#+author: Jack Stephenson

Tools to help when streaming on twitch

#+begin_src dot :file docs/images/architecture.png
digraph {
  subgraph cluster_1 {
    Twitch
    Discord
  }

  subgraph cluster_2 {
    "Community Member"
    Streamer
    Mod
    Admin
  }

  subgraph cluster_0 {
    "Twitch Chat"
    "Discord Chat"
    "Event Stream"
    "Chat Responder"
    Notifier
    Identity
    "Chat Dashboard"
    "Community Dashboard"
    Database
    
    "Twitch Chat" -> "Event Stream"
    "Discord Chat" -> "Event Stream"

    "Event Stream" -> "Chat Responder"

    "Chat Responder" -> "Twitch Chat"
    "Chat Responder" -> "Discord Chat"

    "Event Stream" -> Database
    "Chat Dashboard" -> Database
    "Community Dashboard" -> Database
    Identity -> Database
    "Event Stream" -> Notifier
  }

  Twitch -> "Twitch Chat"
  "Twitch Chat" -> Twitch
  Discord -> "Discord Chat"
  "Discord Chat" -> Discord

  "Community Member" -> "Community Dashboard"
  "Community Member" -> Twitch
  "Community Member" -> Discord
}
#+end_src

#+RESULTS:
[[file:docs/images/architecture.png]]


** Twitch-chat

#+begin_src jsonnet
if message.source == 'twitch'
then (if message.shebang == 'hello' then twitch.message.send(message.author, 'world')) # !hello
else if message.source == 'discord'
then (if message)
#+end_src

Twitch chat provides an API to interface with the chat.
