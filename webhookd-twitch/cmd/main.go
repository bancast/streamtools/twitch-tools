package cmd

import (
	"fmt"
	"os"

	"github.com/spf13/cobra"
	"gitlab.com/bancast/streamtools/twitch-tools/pkg/logging"
	"gitlab.com/bancast/streamtools/twitch-tools/webhookd-twitch/cmd/listen"
)

func NewRootCmd() *cobra.Command {
	cmd := &cobra.Command{
		Use:   "webhookd-twitch",
		Short: "Send webhooks requests based on message triggers",
	}

	logger := logging.SetupLoggerFlags(cmd.PersistentFlags())

	cmd.AddCommand(listen.NewListenCommand(logger))

	return cmd
}

func Execute() {
	rootCmd := NewRootCmd()

	if err := rootCmd.Execute(); err != nil {
		fmt.Fprintln(os.Stderr, err)
		os.Exit(1)
	}
}
