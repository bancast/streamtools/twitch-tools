package jsonnetenv

import (
	"context"
	"io/ioutil"

	jsonnet "github.com/google/go-jsonnet"
)

func NewVM(ctx context.Context, relativePath string, extVars []Var, tlaVars []Var) *VM {
	vm := &VM{jsonnet.MakeVM()}
	vm.Apply(
		ctx,
		WithGitRevisionNativeFunction(),
		WithExtVars(extVars),
		WithTLAVars(tlaVars),
	)
	return vm
}

// NewVMWithOptions creates a new
func NewVMWithOptions(ctx context.Context, relativePath string, options *VMOptions) (*VM, error) {
	vm := &VM{jsonnet.MakeVM()}
	err := vm.Apply(
		ctx,
		WithGitRevisionNativeFunction(),
		WithFileImporter(relativePath, options.JPaths),
		WithExtVars(options.ExtVars),
		WithTLAVars(options.TLAVars),
		WithExtCodes(options.ExtCodes),
		WithTLACodes(options.TLACodes),
	)

	return vm, err
}

type VM struct {
	*jsonnet.VM
}

type VMOptions struct {
	JPaths   []string
	Snapshot bool
	ExtVars  []Var
	ExtCodes []Code
	TLAVars  []Var
	TLACodes []Code
}

func (vm *VM) EvaluateFile(ctx context.Context, path string) (string, error) {
	contents, err := ioutil.ReadFile(path)
	if err != nil {
		return "", err
	}

	return vm.EvaluateSnippet(ctx, path, string(contents))
}

func (vm *VM) EvaluateSnippet(_ context.Context, path string, snippet string) (string, error) {
	output, err := vm.VM.EvaluateSnippet(path, snippet)
	if err != nil {
		return output, err
	}

	return output, nil
}

func (vm *VM) Apply(ctx context.Context, opts ...VMOption) error {
	for _, opt := range opts {
		err := opt(ctx, vm)
		if err != nil {
			return err
		}
	}

	return nil
}
