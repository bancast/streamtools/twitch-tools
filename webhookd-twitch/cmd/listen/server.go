package listen

import (
	"context"

	"github.com/go-logr/logr"
	"google.golang.org/grpc"
	"google.golang.org/grpc/codes"

	dbWebhooks "gitlab.com/bancast/streamtools/twitch-tools/pkg/database/webhooks"
	grpcUtils "gitlab.com/bancast/streamtools/twitch-tools/pkg/grpc"
	"gitlab.com/bancast/streamtools/twitch-tools/pkg/webhooks"
	"gitlab.com/bancast/streamtools/twitch-tools/protos/twitch/webhookd"
)

type WebhookConfigurationServer struct {
	webhookd.UnimplementedWebhookConfigurationServer
	grpcUtils.DisabledAuth
	store  webhooks.Store
	logger logr.Logger
}

func (w WebhookConfigurationServer) Create(ctx context.Context, req *webhookd.CreateRequest) (*webhookd.CreateResponse, error) {
	id, err := w.store.Create(ctx, req.TwitchId, dbWebhooks.Webhook{
		URL:    req.Webhook.Url,
		Secret: req.Webhook.Secret,
	})
	if err != nil {
		return nil, grpc.Errorf(codes.Internal, "failed to create webhook", err)
	}

	return &webhookd.CreateResponse{Id: id}, nil
}

func (w WebhookConfigurationServer) List(ctx context.Context, req *webhookd.ListRequest) (*webhookd.ListResponse, error) {
	webhooks, err := w.store.List(ctx, req.TwitchId)
	if err != nil {
		return nil, grpc.Errorf(codes.Internal, "failed to list webhooks", err)
	}

	protoWebhooks := make([]*webhookd.Webhook, 0, len(webhooks))
	for _, webhook := range webhooks {
		protoWebhooks = append(protoWebhooks, &webhookd.Webhook{
			Id:     webhook.GetID(),
			Url:    webhook.GetURL(),
			Secret: webhook.GetSecret(),
		})
	}

	return &webhookd.ListResponse{
		Webhooks: protoWebhooks,
	}, nil
}

func (w WebhookConfigurationServer) Read(ctx context.Context, req *webhookd.ReadRequest) (*webhookd.Webhook, error) {
	webhook, err := w.store.Read(ctx, req.Id)
	if err != nil {
		return nil, grpc.Errorf(codes.Internal, "failed to read webhook", err)
	}

	return &webhookd.Webhook{
		Id:     webhook.GetID(),
		Url:    webhook.GetURL(),
		Secret: webhook.GetSecret(),
	}, nil
}

func (w WebhookConfigurationServer) Update(ctx context.Context, req *webhookd.UpdateRequest) (*webhookd.UpdateResponse, error) {
	err := w.store.Update(ctx, dbWebhooks.NewWebhook(req.Webhook.Id, req.Webhook.Url, req.Webhook.Secret))
	if err != nil {
		return nil, grpc.Errorf(codes.Internal, "failed to update webhook")
	}

	return &webhookd.UpdateResponse{}, nil
}

func (w WebhookConfigurationServer) Delete(ctx context.Context, req *webhookd.DeleteRequest) (*webhookd.DeleteResponse, error) {
	err := w.store.Delete(ctx, req.Id)
	if err != nil {
		return nil, grpc.Errorf(codes.Internal, "failed to delete webhook")
	}

	return &webhookd.DeleteResponse{}, nil
}
