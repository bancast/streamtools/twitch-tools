package responses

import (
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"regexp"

	"gitlab.com/bancast/streamtools/twitch-tools/pkg/jsonnetenv"
	"gitlab.com/bancast/streamtools/twitch-tools/protos/twitch/messages"
)

func NewResponder(store Store) *Responder {
	return &Responder{
		Store: store,
	}
}

type Responder struct {
	Store Store
}

var ErrNoResponseMatch = errors.New("no responses found")

func (r *Responder) Respond(ctx context.Context, twitch_id string, msg *messages.PrivateMessage) (string, error) {
	responses, err := r.Store.List(ctx, twitch_id)
	if err != nil {
		return "", fmt.Errorf("failed to load responses: %w", err)
	}

	for _, response := range responses {
		exp, err := regexp.Compile(response.GetPattern())
		if err != nil {
			// TODO(jackatbancast): Add some logging / tracing here to expose to the user
			// Skip over failing ones so the bot works
			continue
		}
		matches := exp.FindAllStringSubmatch(msg.Msg, -1)
		if matches == nil {
			// Skip over responses that don't match
			continue
		}

		// If we don't need to create a jsonnet vm then don't
		if response.GetContentType() == ContentTypeString {
			return response.GetContent(), nil
		}

		vm, err := jsonnetenv.NewVMWithOptions(
			ctx,
			".",
			&jsonnetenv.VMOptions{
				JPaths:  nil,
				ExtVars: nil,
				TLAVars: nil,
				TLACodes: []jsonnetenv.Code{
					{Key: "msg", Value: msg},
					{Key: "matches", Value: matches},
				},
			},
		)
		if err != nil {
			return "", fmt.Errorf("failed to create jsonnet vm: %w", err)
		}

		content := response.GetContent()
		if response.GetContentType() == ContentTypeFormatString {
			content = fmt.Sprintf("function (msg, matches) \"%s\" %% {msg: msg, matches: matches}", response.GetContent())
		}

		out, err := vm.EvaluateSnippet(ctx, "<response>", content)
		if err != nil {
			return "", fmt.Errorf("failed to run jsonnet: %w", err)
		}

		var interfaceOut interface{}
		json.Unmarshal([]byte(out), &interfaceOut)
		if returnMessage, ok := interfaceOut.(string); ok {
			return returnMessage, nil
		}

		// It's not an error to not say anything
		return "", nil
	}

	return "", ErrNoResponseMatch
}
