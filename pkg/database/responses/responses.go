package database

import (
	"context"
	"fmt"

	"github.com/google/uuid"
	"github.com/jmoiron/sqlx"
	"gitlab.com/bancast/streamtools/twitch-tools/pkg/responses"
)

var _ responses.Store = &DatabaseResponsesStore{}

const (
	ListQuery string = `
select
  r.id as id,
  bc.id as bot_config_id,
  r.priority as priority,
  r.pattern as pattern,
  r.content_type as content_type,
  r.content as content
from streaming.responses r
inner join streaming.bot_configs bc on bc.id = r.bot_config_id
inner join streaming.users u on u.id = bc.owner_id
  where u.twitch_id = $1
order by r.priority desc;`

	GetConfigIDQuery string = `
select bc.id
from streaming.bot_configs bc
inner join streaming.users u on bc.owner_id = u.id
where u.twitch_id = $1;
`
	CreateQuery string = `
insert into streaming.responses
  (bot_config_id, priority, pattern, content_type, content)
VALUES
  ($1, $2, $3, $4, $5)
returning id;
`
	UpdateQuery string = `
update streaming.responses
set
  priority=$2,
  pattern=$3,
  content_type=$4,
  content=$5
where id = $1;
`
	ReadQuery string = `
select
  id,
  priority,
  pattern,
  content_type,
  content
from streaming.responses
where id = $1;
`
	DeleteQuery string = `
delete
from streaming.responses
where id = $1;
`
)

func NewResponse(id string, priority int, pattern string, contentType responses.ContentType, content string) *Response {
	uid, _ := uuid.Parse(id)
	return &Response{
		ID:          uid,
		Priority:    priority,
		Pattern:     pattern,
		ContentType: contentType,
		Content:     content,
	}
}

type Response struct {
	ID          uuid.UUID `db:"id"`
	BotConfigID uuid.UUID `db:"bot_config_id"`
	Priority    int       `db:"priority"`
	// Regex
	// "!so (.+)"
	Pattern     string                `db:"pattern"`
	ContentType responses.ContentType `db:"content_type"`
	/// Jsonnet code
	// "Hello %(msg.User.DisplayName)s, hope you're having a great day!"
	Content string `db:"content"`
}

func (r Response) GetID() string {
	return r.ID.String()
}

func (r Response) GetPriority() int {
	return r.Priority
}

func (r Response) GetPattern() string {
	return r.Pattern
}

func (r Response) GetContentType() responses.ContentType {
	return r.ContentType
}

func (r Response) GetContent() string {
	return r.Content
}

func NewDatabaseResponsesStore(ctx context.Context, db *sqlx.DB) (*DatabaseResponsesStore, error) {
	getter := &DatabaseResponsesStore{
		DB: db,
	}

	return getter, getter.Health(ctx)
}

type DatabaseResponsesStore struct {
	DB *sqlx.DB
}

func (d *DatabaseResponsesStore) List(ctx context.Context, channel string) ([]responses.Response, error) {
	stmt, err := d.DB.PreparexContext(ctx, ListQuery)
	if err != nil {
		return nil, fmt.Errorf("failed to prepare statement: %w", err)
	}

	responses := []Response{}
	err = stmt.SelectContext(ctx, &responses, channel)
	if err != nil {
		return nil, fmt.Errorf("Failed to query responses for %s: %w", channel, err)
	}

	return mapResponses(responses), nil
}

func mapResponses(inResponses []Response) []responses.Response {
	responses := make([]responses.Response, 0, len(inResponses))
	for _, response := range inResponses {
		responses = append(responses, response)
	}
	return responses
}

func (d *DatabaseResponsesStore) Create(ctx context.Context, twitch_id string, response responses.Response) (string, error) {
	tx, err := d.DB.Beginx()
	stmt, err := tx.PreparexContext(ctx, GetConfigIDQuery)
	if err != nil {
		tx.Rollback()
		return "", fmt.Errorf("failed to prepare statement: %w", err)
	}

	var bot_config_id uuid.UUID
	err = stmt.GetContext(ctx, &bot_config_id, twitch_id)

	stmt, err = tx.PreparexContext(ctx, CreateQuery)
	if err != nil {
		tx.Rollback()
		return "", fmt.Errorf("failed to prepare statement: %w", err)
	}

	var id uuid.UUID
	err = stmt.GetContext(ctx, &id, bot_config_id, response.GetPriority(), response.GetPattern(), response.GetContentType(), response.GetContent())
	if err != nil {
		tx.Rollback()
		return "", fmt.Errorf("failed to create new response: %w", err)
	}

	err = tx.Commit()
	if err != nil {
		tx.Rollback()
		return "", err
	}
	return id.String(), nil
}

func (d *DatabaseResponsesStore) Read(ctx context.Context, id string) (responses.Response, error) {
	uid, err := uuid.Parse(id)
	if err != nil {
		return nil, fmt.Errorf("failed to parse ID (%s) to get: %w", uid, err)
	}

	stmt, err := d.DB.PreparexContext(ctx, ReadQuery)
	if err != nil {
		return nil, fmt.Errorf("failed to prepare statement: %w", err)
	}

	var response Response
	err = stmt.GetContext(ctx, &response, uid)
	if err != nil {
		return nil, fmt.Errorf("failed to get response: %w", err)
	}

	return response, nil
}

func (d *DatabaseResponsesStore) Update(ctx context.Context, response responses.Response) error {
	uid, err := uuid.Parse(response.GetID())
	if err != nil {
		return fmt.Errorf("failed to parse ID (%s) to get: %w", uid, err)
	}

	stmt, err := d.DB.PreparexContext(ctx, UpdateQuery)
	if err != nil {
		return fmt.Errorf("failed to prepare statement: %w", err)
	}

	result, err := stmt.ExecContext(
		ctx,
		uid,
		response.GetPriority(),
		response.GetPattern(),
		response.GetContentType(),
		response.GetContent(),
	)
	if err != nil {
		return fmt.Errorf("failed to get response: %w", err)
	}

	num, err := result.RowsAffected()
	if err != nil {
		return fmt.Errorf("failed to get number of rows affected: %w", err)
	}

	if num != 1 {
		return fmt.Errorf("unexpected number of rows affected: %d", num)
	}

	return nil
}

func (d *DatabaseResponsesStore) Delete(ctx context.Context, id string) error {
	uid, err := uuid.Parse(id)
	if err != nil {
		return fmt.Errorf("failed to parse ID (%s) to get: %w", uid, err)
	}

	stmt, err := d.DB.PreparexContext(ctx, DeleteQuery)
	if err != nil {
		return fmt.Errorf("failed to prepare statement: %w", err)
	}

	_, err = stmt.ExecContext(ctx, uid)
	if err != nil {
		return fmt.Errorf("failed to get response: %w", err)
	}

	return nil
}

func (d *DatabaseResponsesStore) Health(ctx context.Context) error {
	return d.DB.PingContext(ctx)
}
