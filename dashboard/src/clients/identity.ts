// interface User {
//     id: string;
//     createdAt: string;
//     updatedAt: string;
//     bot: bool;
//     twitchID: string;
// };

export default class TokenClient {
    // identity
    baseURL: string = "https://identity.local.streamtools.dev"
    // identity-twitch
    loginURL: string = "https://identity.staging.streamtools.dev"

    constructor(baseURL?: string, loginURL?: string) {
        if (baseURL !== undefined) {
            this.baseURL = baseURL
        }

        if (loginURL !== undefined) {
            this.loginURL = loginURL
        }
    }

    async login() {
        // Generate 2 random strings and append them to get one random string
        const nonce = Math.random().toString(36).substring(2, 12) + Math.random().toString(36).substring(2, 12);

        return {
	    nonce,
	    url: `${this.loginURL}?nonce=${nonce}`
	};
    }

    isAuthenticated() {
        return this.getToken() !== null;
    }

    async self(token: string) {
        const resp = await fetch(
            `${this.baseURL}/v1/user`,
            {
                method: 'GET',
                headers: {
                    'Authorization': `Bearer ${token}`,
                },
            },
        );

	if (!resp.ok) {
	    throw new Error(`failed to get self: ${resp.status}`)
	}

	const respJSON = await resp.json();

	return respJSON;
    }

    async exchange(nonce: string, token_id: string) {
        if (token_id === "") {
            return Promise.reject(new Error('no token in url'))
        }

        const resp = await fetch(
            `${this.baseURL}/v1/exchange`,
            {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json',
                },
                body: JSON.stringify({
                    id: token_id,
                    nonce: nonce,
                })
            }
        );

	if (!resp.ok) {
	    throw new Error(`failed to exchange token: ${resp.status}`)
	}

        const jsonResp = await resp.json();
	if (!jsonResp.token) {
	    throw new Error(`failed to exchange token: failed to get token from json`)
	}

	return jsonResp.token;
    }
}
