package database

import (
	"database/sql"
	"fmt"
	"log"
	"net/url"
	"time"

	"contrib.go.opencensus.io/integrations/ocsql"
	"github.com/google/uuid"
	"github.com/jmoiron/sqlx"
	_ "github.com/lib/pq"
)

type User struct {
	ID        uuid.UUID
	CreatedAt time.Time `db:"created_at"`
	UpdatedAt time.Time `db:"updated_at"`

	TwitchID *string `db:"twitch_id"`
	Username string  `db:"username"`
	Bot      bool    `db:"bot"`
}

type Token struct {
	ID        uuid.UUID `db:"id"`
	CreatedAt time.Time `db:"created_at"`
	UpdatedAt time.Time `db:"updated_at"`

	UserID       uuid.UUID `db:"user_id"`
	AccessToken  string    `db:"access_token"`
	RefreshToken string    `db:"refresh_token"`
}

func Open(uri string) (*sqlx.DB, error) {
	parsedUrl, err := url.Parse(uri)
	if err != nil {
		return nil, fmt.Errorf("failed to parse db url: %w", err)
	}

	driverName, err := ocsql.Register(parsedUrl.Scheme, ocsql.WithAllTraceOptions(), ocsql.WithInstanceName("resources"))
	if err != nil {
		log.Fatalf("unable to register our ocsql driver: %v\n", err)
	}

	db, err := sql.Open(driverName, uri)
	if err != nil {
		return nil, fmt.Errorf("failed to open db: %w", err)
	}

	defer ocsql.RecordStats(db, 5*time.Second)()

	return sqlx.NewDb(db, parsedUrl.Scheme), nil
}
