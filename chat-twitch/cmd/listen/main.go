package listen

import (
	"context"
	"fmt"

	"github.com/go-logr/logr"
	"github.com/nicklaw5/helix"
	"github.com/spf13/cobra"

	"gitlab.com/bancast/streamtools/twitch-tools/pkg/database"
	dbUsers "gitlab.com/bancast/streamtools/twitch-tools/pkg/database/users"
	gateway "gitlab.com/bancast/streamtools/twitch-tools/pkg/grpc"
	"gitlab.com/bancast/streamtools/twitch-tools/pkg/instrumentation"
	"gitlab.com/bancast/streamtools/twitch-tools/pkg/logging"
	"gitlab.com/bancast/streamtools/twitch-tools/pkg/twitch"
	"gitlab.com/bancast/streamtools/twitch-tools/protos/twitch/chat"
	"google.golang.org/grpc"
)

func NewListenCommand(logger *logging.Logger) *cobra.Command {
	cmd := &cobra.Command{
		Use:   "listen",
		Short: "Listen for new messages",
	}

	clientID := cmd.PersistentFlags().String("client-id", "", "Twitch OAuth2 Client ID")
	clientSecret := cmd.PersistentFlags().String("client-secret", "", "Twitch OAuth2 Client Secret")
	redirectURL := cmd.PersistentFlags().String("redirect-url", "", "URL to redirect to after twitch authorisation")

	dbFlags := database.SetupFlags(cmd.PersistentFlags())
	sinkFlags := gateway.SetupClientFlags("sink", cmd.PersistentFlags())
	grpcServerFlags := gateway.SetupServerFlags(cmd.PersistentFlags())
	instrumentationFlags := instrumentation.SetupFlags(cmd.PersistentFlags())

	cmd.RunE = func(cmd *cobra.Command, args []string) error {
		logger := logger.Logr()
		if *clientID == "" {
			return fmt.Errorf("missing twitch --client-id")
		}
		if *clientSecret == "" {
			return fmt.Errorf("missing twitch --client-secret")
		}

		ctx, cancel := context.WithCancel(cmd.Context())
		defer cancel()

		go instrumentationFlags.RunMetricsServer(ctx, logger.WithName("metrics"))

		logger.V(3).Info("creating helix client")
		helixOptions := &helix.Options{
			ClientID:     *clientID,
			ClientSecret: *clientSecret,
			RedirectURI:  *redirectURL,
		}
		logger.Info("created helix client")

		db := dbFlags.MustDatabase(ctx, logger)
		defer db.Close()

		logger.V(3).Info("creating store adaptors")
		authStore := database.NewAuthStore(ctx, db)
		userStore, _ := dbUsers.NewUserStore(ctx, db)
		controller, err := twitch.NewController(helixOptions, authStore, userStore)
		if err != nil {
			return fmt.Errorf("failed to create twitch controller: %w", err)
		}
		logger.Info("created store adaptors")

		sinkGrpcClient := sinkFlags.MustClient(ctx, logger)
		defer sinkGrpcClient.Close()
		logger.Info("creating twitch sink adaptor")
		sink := NewTwitchSinker(sinkGrpcClient)

		logger.Info("spinning off bot controller")
		loginChan := make(chan string)
		go Bot(ctx, logger.WithName("bot-ingress"), controller, sink, loginChan)

		logger.Info("sending login events to the bot controller")
		// Start by watching all of the users
		users, err := controller.GetUsers(ctx)
		if err != nil {
			cancel()
			return fmt.Errorf("failed to get user list: %w", err)
		}
		for _, user := range users {
			logger.Info("sending login event", "user_id", user.GetTwitchID())
			loginChan <- user.GetTwitchID()
		}

		logger.Info("spinning up API")
		go NewGrpc(ctx, logger.WithName("api"), grpcServerFlags, controller)

		logger.Info("waiting for requests...")
		<-ctx.Done()

		logger.Info("context complete; shutting down")
		close(loginChan)
		return nil
	}

	return cmd
}

func NewGrpc(ctx context.Context, logger logr.Logger, serverFlags *gateway.ServerFlags, controller *twitch.Controller) error {
	logger.V(3).Info("setting up server")
	server := NewChatServer(logger, controller)
	logger.Info("setting up gateway runner")
	err := serverFlags.RegisterGateway(
		ctx, logger,
		func(ctx context.Context, grpcServer *grpc.Server) error {
			logger.Info("registering grpc server")
			chat.RegisterTwitchChatServer(grpcServer, server)
			return nil
		},
		chat.RegisterTwitchChatHandlerFromEndpoint,
	)
	if err != nil {
		logger.Error(err, "failed to run gateway")
		return fmt.Errorf("failed to run with gateway: %w", err)
	}

	logger.Info("finished setting up gateway")
	return serverFlags.Listen(ctx, logger.WithName("server"))
}
