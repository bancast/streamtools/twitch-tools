create schema if not exists streaming;
grant all on schema streaming to postgres;
grant all on schema streaming to public;

set search_path to streaming;

create extension if not exists "uuid-ossp";

create table if not exists streaming.users (
       id UUID primary key default uuid_generate_v4() not null,
       created_at timestamp default now() not null,
       updated_at timestamp default now() not null,

       twitch_id varchar(64) unique not null,
       username varchar(50) unique not null,
       bot boolean default false
);

create table if not exists streaming.tokens (
       id UUID primary key default uuid_generate_v4() not null,
       created_at timestamp default now() not null,
       updated_at timestamp default now() not null,
       
       user_id UUID not null references users(id) on delete cascade,
       access_token varchar(255) not null,
       refresh_token varchar(255) not null
);

create table if not exists streaming.user_tokens (
       id UUID primary key default uuid_generate_v4() not null,
       created_at timestamp default now() not null,
       last_used_at timestamp,

       nonce varchar(128) not null,
       user_id UUID unique not null references users(id) on delete cascade,
       token_name varchar(50) not null,

       expiry timestamp,
       lifetime int,

       exchange_by timestamp not null,
       exchanged boolean default false not null,

       scopes varchar(50)[] not null,
       comment varchar(250)
);

create table if not exists streaming.bot_configs (
       id UUID primary key default uuid_generate_v4() not null,       
       created_at timestamp default now() not null,
       updated_at timestamp default now() not null,

       owner_id UUID not null references users(id) on delete cascade,
       user_id UUID references users(id) on delete set null
);

do $$
begin
if not exists (select 1 from pg_type where typname = 'response_content_t') then
   create type streaming.response_content_t as enum ('string', 'formatted', 'jsonnet');
end if;
end
$$;

create table if not exists streaming.responses (
       id UUID primary key default uuid_generate_v4() not null,
       created_at timestamp default now() not null,
       updated_at timestamp default now() not null,
       
       bot_config_id UUID not null references bot_configs(id) on delete cascade,

       priority int not null,
       pattern text not null,
       content_type response_content_t default 'formatted' not null,
       content text not null
);


create table if not exists streaming.webhook_configs (
	   id UUID primary key default uuid_generate_v4() not null,
	   created_at timestamp default now() not null,
	   updated_at timestamp default now() not null,

	   owner_id UUID not null references users(id) on delete cascade
);

create table if not exists streaming.webhooks (
	   id UUID primary key default uuid_generate_v4() not null,
	   created_at timestamp default now() not null,
	   updated_at timestamp default now() not null,

	   webhook_config_id UUID not null references webhook_configs(id) on delete cascade,

	   url varchar(1024) not null,
	   secret varchar(256) not null
);
