package tokens

import (
	"context"
	"fmt"
	"time"

	"github.com/google/uuid"
	"github.com/jmoiron/sqlx"
	"github.com/lib/pq"
	"gitlab.com/bancast/streamtools/twitch-tools/pkg/tokens"
)

var _ tokens.Store = &DatabaseTokenStore{}

const (
	ListQuery string = `
select
  *
from streaming.user_tokens
order by last_used_at desc;
`
	CreateQuery string = `
insert into streaming.user_tokens
  (nonce, user_id, token_name, lifetime, exchange_by, scopes, comment)
VALUES
  ($1, $2, $3, $4, $5, $6, $7)
returning id;
`
	ReadQuery string = `
select
  *
from streaming.user_tokens
where id = $1;
`
	ExchangeQuery string = `
update streaming.user_tokens
set
  exchanged = true,
  expiry = $3
where
  id=$1 and 
  nonce=$2 and
  exchange_by > now() and
  exchanged = false;
`
	RefreshQuery string = `
update streaming.user_tokens
set
  expiry = $3
where
  id = $1 and
  expiry > $2 and
  exchanged = true;
`
)

const (
	CreationTimeout time.Duration = 5 * time.Minute
)

type Token struct {
	ID     uuid.UUID `db:"id"`
	UserID uuid.UUID `db:"user_id"`
	Nonce  string    `db:"nonce"`

	CreatedAt time.Time `db:"created_at"`
	// Last used at is updated by workers on a queue processing
	// events to represent the time that this token was last seen
	LastUsedAt *time.Time `db:"last_used_at"`

	Name    string `db:"token_name"`
	Comment string `db:"comment"`

	Expiry *time.Time `db:"expiry"`
	// Duration stored as an int (ns)
	Lifetime int64 `db:"lifetime"`

	Exchanged  bool      `db:"exchanged"`
	ExchangeBy time.Time `db:"exchange_by"`

	Scopes *pq.StringArray `db:"scopes"`
}

func (t Token) GetTokenID() string {
	return t.ID.String()
}

func (t Token) GetCreatedAt() time.Time {
	return t.CreatedAt
}

func (t Token) GetLastUsedAt() time.Time {
	if t.LastUsedAt == nil {
		return time.Unix(0, 0)
	}
	return *t.LastUsedAt
}

func (t Token) GetNonce() string {
	return t.Nonce
}

func (t Token) GetUserID() string {
	return t.UserID.String()
}

func (t Token) GetExpiry() time.Time {
	if t.Expiry == nil {
		return time.Unix(0, 0)
	}
	return *t.Expiry
}

func (t Token) GetLifetime() time.Duration {
	return time.Duration(t.Lifetime) * time.Second
}

func (t Token) GetExchanged() bool {
	return t.Exchanged
}

func (t Token) GetExchangeBy() time.Time {
	return t.ExchangeBy
}

func (t Token) GetName() string {
	return t.Name
}

func (t Token) GetComment() string {
	return t.Comment
}

func (t Token) GetScopes() []string {
	return *(*[]string)(t.Scopes)
}

func NewTokenStore(ctx context.Context, db *sqlx.DB) (*DatabaseTokenStore, error) {
	getter := &DatabaseTokenStore{
		DB: db,
	}

	return getter, getter.Health(ctx)
}

type DatabaseTokenStore struct {
	DB *sqlx.DB
}

// List all of the tokens in the database
//
// TODO: list all of the tokens that the token making the request has access to.
func (d *DatabaseTokenStore) List(ctx context.Context) ([]tokens.Token, error) {
	stmt, err := d.DB.PreparexContext(ctx, ListQuery)
	if err != nil {
		return nil, fmt.Errorf("failed to prepare statement: %w", err)
	}

	tokens := []Token{}
	err = stmt.SelectContext(ctx, &tokens)
	if err != nil {
		return nil, fmt.Errorf("Failed to query users: %w", err)
	}

	return mapTokens(tokens), nil
}

func mapTokens(inTokens []Token) []tokens.Token {
	tokens := make([]tokens.Token, 0, len(inTokens))
	for _, token := range inTokens {
		tokens = append(tokens, token)
	}
	return tokens
}

// Create an un-exchanged token and return the ID to later be exchanged
func (d *DatabaseTokenStore) Create(ctx context.Context, options *tokens.CreateOptions) (string, error) {
	stmt, err := d.DB.PreparexContext(ctx, CreateQuery)
	if err != nil {
		return "", fmt.Errorf("failed to prepare statement: %w", err)
	}

	var token_id string
	err = stmt.GetContext(
		ctx, &token_id,
		options.Nonce, options.User, options.Name,
		options.Lifetime/time.Second,
		// Exchange within the timeout (5m)
		time.Now().UTC().Add(CreationTimeout),
		(*pq.StringArray)(&options.Scopes), options.Comment,
	)
	if err != nil {
		return "", fmt.Errorf("failed to create new token request: %w", err)
	}

	return token_id, nil
}

func (d *DatabaseTokenStore) Read(ctx context.Context, options *tokens.ReadOptions) (tokens.Token, error) {
	uid, err := uuid.Parse(options.ID)
	if err != nil {
		return nil, fmt.Errorf("failed to parse ID (%s) to get: %w", uid, err)
	}

	stmt, err := d.DB.PreparexContext(ctx, ReadQuery)
	if err != nil {
		return nil, fmt.Errorf("failed to prepare statement: %w", err)
	}

	var token Token
	err = stmt.GetContext(ctx, &token, uid)
	if err != nil {
		return nil, fmt.Errorf("failed to get token: %w", err)
	}

	return token, nil
}

type Transaction struct {
	tx *sqlx.Tx
	// Cached options
	options tokens.Options
}

// Get the Options for the token transaction (e.g. token id / nonce)
func (tx Transaction) Options() tokens.Options {
	return tx.options
}

// Complete the transaction and commit the changes
func (tx Transaction) Complete() error {
	return tx.tx.Commit()
}

// Error will rollback the transaction and return any error it receives, as well as the error passed in. This will always return error
func (tx Transaction) Error() error {
	return tx.tx.Rollback()
}

// getOptions returns the options for our token exchange. We need
// these in the token provisioner.
func (d *DatabaseTokenStore) getOptions(ctx context.Context, tx *sqlx.Tx, id uuid.UUID) (*tokens.Options, error) {
	stmt, err := tx.PreparexContext(ctx, ReadQuery)
	if err != nil {
		return nil, fmt.Errorf("failed to prepare statement: %w", err)
	}

	var token Token
	err = stmt.GetContext(ctx, &token, id)
	if err != nil {
		return nil, fmt.Errorf("failed to get token: %w", err)
	}

	return &tokens.Options{
		TokenID:  token.GetTokenID(),
		UserID:   token.GetUserID(),
		Expiry:   token.GetExpiry(),
		Lifetime: token.GetLifetime(),
		Scopes:   token.GetScopes(),
	}, nil
}

func (d *DatabaseTokenStore) Exchange(ctx context.Context, options *tokens.ExchangeOptions) (tokens.Transaction, error) {
	tid, err := uuid.Parse(options.ID)
	if err != nil {
		return nil, fmt.Errorf("failed to parse ID (%s) to get: %w", tid, err)
	}

	tx, err := d.DB.Beginx()
	if err != nil {
		return nil, fmt.Errorf("failed to begin transaction: %w", err)
	}

	stmt, err := tx.PreparexContext(ctx, ExchangeQuery)
	if err != nil {
		tx.Rollback()
		return nil, fmt.Errorf("failed to prepare statement: %w", err)
	}

	exchangeOptions, err := d.getOptions(ctx, tx, tid)
	if err != nil {
		tx.Rollback()
		return nil, fmt.Errorf("failed to get exchange options: %w", err)
	}

	result, err := stmt.ExecContext(
		ctx, tid,
		options.Nonce,
		// Expiry
		time.Now().Add(exchangeOptions.Lifetime).UTC(),
	)
	if err != nil {
		tx.Rollback()
		return nil, fmt.Errorf("failed to set exchanged on token: %w", err)
	}

	num, err := result.RowsAffected()
	if err != nil {
		tx.Rollback()
		return nil, fmt.Errorf("failed to get number of rows affected: %w", err)
	}

	if num != 1 {
		tx.Rollback()
		return nil, fmt.Errorf("unexpected number of rows affected: %d", num)
	}

	return &Transaction{tx: tx, options: *exchangeOptions}, nil
}

func (d *DatabaseTokenStore) Refresh(ctx context.Context, options *tokens.RefreshOptions) (tokens.Transaction, error) {
	tid, err := uuid.Parse(options.TokenID)
	if err != nil {
		return nil, fmt.Errorf(
			"failed to parse Token ID (%s) to get: %w",
			options.TokenID, err,
		)
	}

	uid, err := uuid.Parse(options.UserID)
	if err != nil {
		return nil, fmt.Errorf(
			"failed to parse User ID (%s) to get: %w",
			options.UserID, err,
		)
	}

	tx, err := d.DB.Beginx()

	refreshOptions, err := d.getOptions(ctx, tx, tid)

	stmt, err := tx.PreparexContext(ctx, RefreshQuery)
	if err != nil {
		tx.Rollback()
		return nil, fmt.Errorf("failed to prepare statement: %w", err)
	}

	_, err = stmt.ExecContext(ctx, tid, uid)
	if err != nil {
		tx.Rollback()
		return nil, fmt.Errorf("failed to update expiry: %w", err)
	}

	return &Transaction{tx: tx, options: *refreshOptions}, nil
}

func (d *DatabaseTokenStore) Health(ctx context.Context) error {
	return d.DB.PingContext(ctx)
}
