package main

import "gitlab.com/bancast/streamtools/twitch-tools/identity-twitch/cmd"

func main() {
	cmd.Execute()
}
