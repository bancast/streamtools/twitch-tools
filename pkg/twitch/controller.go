package twitch

import (
	"context"
	"fmt"

	"github.com/nicklaw5/helix"
	"gitlab.com/bancast/streamtools/twitch-tools/pkg/auth"
	"gitlab.com/bancast/streamtools/twitch-tools/pkg/users"
)

type Controller struct {
	helixOptions *helix.Options
	HelixClient  *helix.Client
	Auth         auth.TokenStore
	Users        auth.UserStore
}

func NewController(helixOptions *helix.Options, authStore auth.TokenStore, userStore auth.UserStore) (*Controller, error) {
	helixClient, err := helix.NewClient(helixOptions)
	if err != nil {
		return nil, fmt.Errorf("failed to create helix client: %w", err)
	}

	return &Controller{
		HelixClient:  helixClient,
		helixOptions: helixOptions,
		Auth:         authStore,
		Users:        userStore,
	}, nil
}

func (c *Controller) GetUsers(ctx context.Context) ([]users.User, error) {
	return c.Users.List(ctx)
}

func (c *Controller) UserClient(ctx context.Context, twitch_id string) (*Client, error) {
	tokens, err := c.Auth.Get(ctx, twitch_id)
	if err != nil {
		return nil, fmt.Errorf("failed to get user token from database: %w", err)
	}

	return c.Client(ctx, tokens)
}

func (c *Controller) Client(ctx context.Context, tokens *auth.Tokens) (*Client, error) {
	helixClient, err := c.newHelixClient(tokens)
	if err != nil {
		return nil, fmt.Errorf("failed to create helix client: %w")
	}

	client := &Client{
		Controller: c,
		Helix:      helixClient,
		Tokens:     tokens,
	}

	return client, nil
}

func (c *Controller) newHelixClient(tokens *auth.Tokens) (*helix.Client, error) {
	options := *c.helixOptions
	options.UserAccessToken = tokens.AccessToken
	return helix.NewClient(&options)
}

type Client struct {
	*Controller

	Tokens *auth.Tokens
	Helix  *helix.Client
}

func (c *Client) Self(ctx context.Context) (*helix.User, error) {
	users, err := c.Helix.GetUsers(&helix.UsersParams{})
	if err != nil {
		return nil, fmt.Errorf("failed to get user: %w", err)
	}

	if len(users.Data.Users) != 1 {
		return nil, fmt.Errorf("failed to get users: no user found")
	}

	return &users.Data.Users[0], nil
}

func (c *Client) StoreToken(ctx context.Context, twitch_id string) error {
	err := c.Controller.Auth.Store(ctx, twitch_id, *c.Tokens)
	if err != nil {
		return fmt.Errorf("failed to store tokens: %w", err)
	}

	return nil
}

func (c *Client) MaybeRefresh(ctx context.Context) error {
	if _, err := c.Self(ctx); err != nil {
		return c.RefreshToken(ctx)
	}

	return nil
}

func (c *Client) RefreshToken(ctx context.Context) error {
	tokenResponse, err := c.Helix.RefreshUserAccessToken(c.Tokens.RefreshToken)
	if err != nil {
		return fmt.Errorf("failed to refresh token: %w", err)
	}

	c.Tokens = &auth.Tokens{
		AccessToken:  tokenResponse.Data.AccessToken,
		RefreshToken: tokenResponse.Data.RefreshToken,
	}
	c.Helix.SetUserAccessToken(c.Tokens.AccessToken)
	user, err := c.Self(ctx)
	if err != nil {
		return fmt.Errorf("failed to find self: %w", err)
	}

	err = c.StoreToken(ctx, user.ID)
	if err != nil {
		return err
	}

	return nil
}
