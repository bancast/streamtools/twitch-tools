package grpc

import (
	"context"
	"errors"
	"fmt"

	grpc_auth "github.com/grpc-ecosystem/go-grpc-middleware/auth"
	"gitlab.com/bancast/streamtools/twitch-tools/pkg/tokens"
	"google.golang.org/grpc"
)

var _ grpc_auth.ServiceAuthFuncOverride = &DisabledAuth{}

type DisabledAuth struct{}

func (_ DisabledAuth) AuthFuncOverride(ctx context.Context, method string) (context.Context, error) {
	return ctx, nil
}

var ErrorNoMethod error = errors.New("no method found in context")

func OverrideAuthFunc(override grpc_auth.ServiceAuthFuncOverride) grpc_auth.AuthFunc {
	return func(ctx context.Context) (context.Context, error) {
		method, ok := grpc.Method(ctx)
		if !ok {
			return nil, ErrorNoMethod
		}

		ctx, err := override.AuthFuncOverride(ctx, method)
		if err != nil {
			return nil, fmt.Errorf("faild auth: %w", err)
		}

		return ctx, nil
	}
}

type PermissiveAuth struct {
	authFunc         grpc_auth.AuthFunc
	allowedMethodMap map[string]struct{}
}

func (pa *PermissiveAuth) AuthFuncOverride(ctx context.Context, method string) (context.Context, error) {
	if _, ok := pa.allowedMethodMap[method]; ok {
		return ctx, nil
	}
	return pa.authFunc(ctx)
}

var _ grpc_auth.ServiceAuthFuncOverride = &PermissiveAuth{}

func MakePermissiveAuthFunc(provisioner tokens.ContextProvisioner, allowedMethods []string) *PermissiveAuth {
	// Build permissive map ahead of time
	allowedMethodMap := map[string]struct{}{}
	for _, method := range allowedMethods {
		allowedMethodMap[method] = struct{}{}
	}

	return &PermissiveAuth{
		authFunc:         MakeAuthFunc(provisioner),
		allowedMethodMap: allowedMethodMap,
	}
}

func MakeAuthFunc(provisioner tokens.ContextProvisioner) func(context.Context) (context.Context, error) {
	return func(ctx context.Context) (context.Context, error) {
		if grpcMethod, ok := grpc.Method(ctx); ok && grpcMethod == "/grpc.reflection.v1alpha.ServerReflection/ServerReflectionInfo" {
			return ctx, nil
		}
		token, err := grpc_auth.AuthFromMD(ctx, "bearer")
		if err != nil {
			return nil, err
		}

		ctx, err = provisioner.SetToken(ctx, token)
		if err != nil {
			return nil, fmt.Errorf("failed to authenticate: %w", err)
		}

		return ctx, nil
	}

}
