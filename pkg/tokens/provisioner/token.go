package provisioner

import (
	"time"
)

type Token StandardClaimsWithScopes

func (t Token) GetTokenID() string {
	return t.Id
}

func (t Token) GetCreatedAt() time.Time {
	return time.Unix(t.IssuedAt, 0)
}

func (t Token) GetLastUsedAt() time.Time {
	return time.Now().UTC()
}

func (t Token) GetNonce() string {
	return ""
}

func (t Token) GetUserID() string {
	return t.Subject
}

func (t Token) GetExpiry() time.Time {
	return time.Unix(t.ExpiresAt, 0)
}

func (t Token) GetLifetime() time.Duration {
	return t.GetExpiry().Sub(t.GetCreatedAt())
}

func (t Token) GetExchanged() bool {
	return true
}

func (t Token) GetExchangeBy() time.Time {
	return time.Time{}
}

func (t Token) GetName() string {
	return ""
}

func (t Token) GetComment() string {
	return ""
}

func (t Token) GetScopes() []string {
	return t.Scopes
}
