package listen

import (
	"context"
	"fmt"
	"log"

	"github.com/spf13/cobra"
	"gitlab.com/bancast/streamtools/twitch-tools/pkg/logging"
	"gitlab.com/bancast/streamtools/twitch-tools/pkg/pubsub"
	"gitlab.com/bancast/streamtools/twitch-tools/protos/twitch/messages"
	"google.golang.org/protobuf/encoding/prototext"
)

func NewListenCommand(logger *logging.Logger) *cobra.Command {
	cmd := &cobra.Command{
		Use:   "listen",
		Short: "Listen for new webhook pushes",
	}

	pubsubFlags := pubsub.SetupFlags(cmd.PersistentFlags())

	cmd.RunE = func(cmd *cobra.Command, args []string) error {
		logger := logger.Logr()

		ctx, cancel := context.WithCancel(cmd.Context())
		defer cancel()

		ch, closeCh := pubsubFlags.MustChannel(ctx, logger)
		defer closeCh()

		msgs := make(chan *messages.TwitchMessage)

		logger.Info("starting message read loop")
		go pubsub.ReadTwitchMessages(ctx, ch, "log", msgs)

		logger.Info("starting message logging")
		err := ReadMessages(ctx, msgs)
		if err != nil {
			return fmt.Errorf("failed to read messages: %w", err)
		}

		return nil
	}

	return cmd
}

func ReadMessages(ctx context.Context, msgs <-chan *messages.TwitchMessage) error {
	for msg := range msgs {
		baseMessage := msg.GetMsg()
		if baseMessage == nil {
			continue
		}

		if privMsg, ok := baseMessage.(*messages.TwitchMessage_PrivateMessage); ok {
			log.Printf(
				"%s: %s\n",
				privMsg.PrivateMessage.User.DisplayName,
				privMsg.PrivateMessage.Msg,
			)
			continue
		} else if joinMsg, ok := baseMessage.(*messages.TwitchMessage_UserJoinMessage); ok {
			log.Printf(
				"%s joined the channel %s\n",
				joinMsg.UserJoinMessage.User,
				joinMsg.UserJoinMessage.Channel,
			)
		} else {
			out, err := prototext.Marshal(msg)
			if err != nil {
				log.Printf("failed to marshal message: %s", err)
				continue
			}
			log.Println(string(out))
		}
	}
	return nil
}
