PROG_NAME := manifesto
SERVICES := chat-twitch dashboard identity identity-twitch log-twitch responder-twitch shell-twitch sink-twitch webhookd webhookd-twitch

.PHONY: all
all: build

.PHONY: build
build:
	bazel build //...

.PHONY: upload
upload: build
	@for service in $(SERVICES); do \
		bazel run  //$${service}:$${service}-image-push --platforms=@io_bazel_rules_go//go/toolchain:linux_amd64 --embed_label $(shell git rev-parse HEAD); \
	done;

.PHONY: test
test:
	bazel test //...

.PHONY: bazel-gazelle
bazel-gazelle:
	bazel run //:gazelle

.PHONY: bazel-gazelle-update-repos
bazel-gazelle-update-repos:
	bazel run //:gazelle -- update-repos -from_file=go.mod -to_macro=bazel/deps.bzl%go_dependencies

