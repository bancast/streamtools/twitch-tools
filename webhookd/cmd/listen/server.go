package listen

import (
	"context"
	"io/ioutil"
	"net/http"
	"strings"
	"time"

	"github.com/go-logr/logr"
	"github.com/streadway/amqp"
	"google.golang.org/grpc"
	"google.golang.org/grpc/codes"

	grpcUtils "gitlab.com/bancast/streamtools/twitch-tools/pkg/grpc"
	"gitlab.com/bancast/streamtools/twitch-tools/protos/webhookd"
)

type WebhookServer struct {
	webhookd.UnimplementedWebhookServiceServer
	grpcUtils.DisabledAuth
	channel *amqp.Channel
	logger  logr.Logger
}

func (t WebhookServer) Push(ctx context.Context, req *webhookd.PushRequest) (*webhookd.PushResponse, error) {
	logger := t.logger

	logger.V(3).Info("creating http transport")
	tr := &http.Transport{
		MaxIdleConns:    10,
		IdleConnTimeout: 5 * time.Second,
	}
	logger.V(3).Info("creating http client")
	client := &http.Client{Transport: tr}

	logger.Info("posting webhook", "url", req.Url)
	resp, err := client.Post(req.Url, "application/json", strings.NewReader(req.Body))
	if err != nil {
		err := grpc.Errorf(codes.Internal, "failed to connect to server")
		logger.Error(err, "failed to connect to remote server", "url", req.Url)
		return nil, err
	}
	defer resp.Body.Close()

	if resp.StatusCode >= 400 || resp.StatusCode < 200 {
		logger.Info("got back invalid response", "status_code", resp.StatusCode)
		return nil, grpc.Errorf(codes.Internal, "failed to make a valid request")
	}

	logger.Info("reading response body")
	data, _ := ioutil.ReadAll(resp.Body)

	logger.Info("relaying response")
	return &webhookd.PushResponse{
		Sent:     true,
		Response: string(data),
	}, nil
}
