package listen

import (
	"context"
	"errors"
	"fmt"
	"time"

	"github.com/go-logr/logr"
	"github.com/jmoiron/sqlx"
	"github.com/spf13/cobra"
	"gitlab.com/bancast/streamtools/twitch-tools/pkg/database"
	dbWebhooks "gitlab.com/bancast/streamtools/twitch-tools/pkg/database/webhooks"
	gateway "gitlab.com/bancast/streamtools/twitch-tools/pkg/grpc"
	"gitlab.com/bancast/streamtools/twitch-tools/pkg/logging"
	"gitlab.com/bancast/streamtools/twitch-tools/pkg/pubsub"
	"gitlab.com/bancast/streamtools/twitch-tools/pkg/responses"
	"gitlab.com/bancast/streamtools/twitch-tools/pkg/webhooks"
	"gitlab.com/bancast/streamtools/twitch-tools/protos/twitch/messages"
	twitchWebhookd "gitlab.com/bancast/streamtools/twitch-tools/protos/twitch/webhookd"
	"gitlab.com/bancast/streamtools/twitch-tools/protos/webhookd"
	"google.golang.org/grpc"
	"google.golang.org/protobuf/encoding/protojson"
)

const (
	QueueName string = "webhook"
)

func NewListenCommand(logger *logging.Logger) *cobra.Command {
	cmd := &cobra.Command{
		Use:   "listen",
		Short: "Listen for new messages",
	}

	dbFlags := database.SetupFlags(cmd.PersistentFlags())
	pubsubFlags := pubsub.SetupFlags(cmd.PersistentFlags())
	webhookdFlags := gateway.SetupClientFlags("webhookd", cmd.PersistentFlags())
	grpcServerFlags := gateway.SetupServerFlags(cmd.PersistentFlags())

	cmd.RunE = func(cmd *cobra.Command, args []string) error {
		logger := logger.Logr()

		ctx, cancel := context.WithCancel(cmd.Context())
		defer cancel()

		ch, closeCh := pubsubFlags.MustChannel(ctx, logger)
		defer closeCh()

		db := dbFlags.MustDatabase(ctx, logger)

		logger.Info("creating store abstractions")
		webhookStore, err := NewWebhookStore(ctx, db)
		if err != nil {
			return fmt.Errorf("failed to create webhook handler: %w", err)
		}

		logger.Info("starting configuration api")
		go ConfigurationAPI(ctx, logger.WithName("config"), grpcServerFlags, webhookStore)

		webhookdGRPCClient := webhookdFlags.MustClient(ctx, logger)
		defer webhookdGRPCClient.Close()
		logger.Info("creating client", "server", "webhookd")
		webhookdClient := webhookd.NewWebhookServiceClient(webhookdGRPCClient)

		msgChan := make(chan *messages.TwitchMessage)
		logger.Info("starting responding to messages")
		go respond(ctx, logger, webhookStore, webhookdClient, msgChan)

		logger.Info("starting message ingestion")
		return pubsub.ReadTwitchMessages(ctx, ch, QueueName, msgChan)
	}

	return cmd
}

func respond(ctx context.Context, logger logr.Logger, webhookStore webhooks.Store, webhookdClient webhookd.WebhookServiceClient, msgChan <-chan *messages.TwitchMessage) error {
	logger.Info("starting message read loop")
	for msg := range msgChan {
		logger.Info("reading message")
		baseMessage := msg.GetMsg()
		if baseMessage == nil {
			logger.Info("empty message, skipping")
			continue
		}

		privMsg, ok := baseMessage.(*messages.TwitchMessage_PrivateMessage)
		if !ok {
			logger.Info("not a private message, skipping")
			continue
		}

		logger := logger.WithValues(
			"channel", privMsg.PrivateMessage.Channel,
			"msg", privMsg.PrivateMessage.Msg,
		)

		logger.Info("getting list of webhooks for message")
		whList, err := webhookStore.List(ctx, privMsg.PrivateMessage.Channel)
		if err != nil && !errors.Is(err, responses.ErrNoResponseMatch) {
			logger.Error(err, "failed to get list of webhooks")
		}

		logger.V(3).Info("marshalling message to json")
		bodyBytes, err := protojson.Marshal(msg)
		if err != nil {
			logger.Error(err, "failed to marshal message to json")
			continue
		}

		for _, wh := range whList {
			logger := logger.WithValues("webhook", wh.GetID())

			ctx, cancel := context.WithTimeout(ctx, 5*time.Second)

			logger.Info("sending webhook request")
			resp, err := webhookdClient.Push(ctx, &webhookd.PushRequest{
				Url:    wh.GetURL(),
				Secret: wh.GetSecret(),
				Body:   string(bodyBytes),
			})
			if err != nil {
				logger.Error(err, "failed to push webhook")
			}
			if resp.Sent == false {
				logger.Info("failed to send webhook", "response", resp.Response)
			}
			cancel()
		}
	}

	return nil
}

func NewWebhookStore(ctx context.Context, db *sqlx.DB) (webhooks.Store, error) {
	webhookStore, err := dbWebhooks.NewWebhookStore(ctx, db)
	if err != nil {
		return nil, fmt.Errorf("failed to create database getter: %w", err)
	}

	return webhookStore, nil
}

func ConfigurationAPI(ctx context.Context, logger logr.Logger, serverFlags *gateway.ServerFlags, store webhooks.Store) error {
	logger.Info("creating configuration server")
	server := WebhookConfigurationServer{store: store, logger: logger.WithName("server")}
	logger.Info("starting server", "server", "gateway")
	err := serverFlags.RegisterGateway(
		ctx, logger,
		func(ctx context.Context, grpcServer *grpc.Server) error {
			logger.Info("registering grpc server")
			twitchWebhookd.RegisterWebhookConfigurationServer(grpcServer, server)
			return nil
		},
		twitchWebhookd.RegisterWebhookConfigurationHandlerFromEndpoint,
	)
	if err != nil {
		return fmt.Errorf("failed to run with gateway: %w", err)
	}

	return serverFlags.Listen(ctx, logger.WithName("server"))
}
