package grpc

import (
	"context"
	"errors"
	"fmt"

	"gitlab.com/bancast/streamtools/twitch-tools/pkg/users"
	"gitlab.com/bancast/streamtools/twitch-tools/protos/twitch/identity"
	"google.golang.org/grpc"
	"google.golang.org/protobuf/types/known/timestamppb"
)

var _ users.Controller = &Client{}

var (
	ErrorUnimplemented error = errors.New("unimplemented")
)

func NewClient(conn grpc.ClientConnInterface) *Client {
	return &Client{
		UserManagementClient: identity.NewUserManagementClient(conn),
	}
}

type Client struct {
	identity.UserManagementClient
}

func (c *Client) List(ctx context.Context) ([]users.User, error) {
	ctx, cancel := context.WithCancel(ctx)
	defer cancel()

	resp, err := c.UserManagementClient.List(ctx, &identity.ListRequest{})
	if err != nil {
		return nil, fmt.Errorf("failed to lookup user: %w", err)
	}

	return mapUsers(resp.Users), nil
}

func mapUsers(protoUsers []*identity.User) []users.User {
	users := make([]users.User, 0, len(protoUsers))

	for _, u := range protoUsers {
		users = append(users, convertUser(u))
	}

	return users
}

func (c *Client) Create(ctx context.Context, user users.User) (users.User, error) {
	ctx, cancel := context.WithCancel(ctx)
	defer cancel()

	protoUser, err := c.UserManagementClient.Create(ctx, &identity.CreateRequest{
		TwitchId: user.GetTwitchID(),
		Username: user.GetUsername(),
		Bot:      user.GetBot(),
	})
	if err != nil {
		return nil, fmt.Errorf("failed to create user on gRPC call: %w", err)
	}

	return convertUser(protoUser), nil
}

func (c *Client) Read(ctx context.Context, id string) (users.User, error) {
	ctx, cancel := context.WithCancel(ctx)
	defer cancel()

	resp, err := c.UserManagementClient.Read(ctx, &identity.ReadRequest{
		Id: id,
	})
	if err != nil {
		return nil, fmt.Errorf("failed to lookup user: %w", err)
	}

	return convertUser(resp), nil
}

func (c *Client) Lookup(ctx context.Context, username string) (users.User, error) {
	ctx, cancel := context.WithCancel(ctx)
	defer cancel()

	resp, err := c.UserManagementClient.Lookup(ctx, &identity.LookupRequest{
		Username: username,
	})
	if err != nil {
		return nil, fmt.Errorf("failed to lookup user: %w", err)
	}

	return convertUser(resp), nil
}

func convertUser(u *identity.User) users.User {
	return &User{
		Id:        u.Id,
		CreatedAt: u.CreatedAt.AsTime(),
		UpdatedAt: u.UpdatedAt.AsTime(),
		Username:  u.Username,
		Bot:       u.Bot,
	}
}

func userToProto(u users.User) *identity.User {
	return &identity.User{
		Id:        u.GetID(),
		CreatedAt: timestamppb.New(u.GetCreatedAt()),
		UpdatedAt: timestamppb.New(u.GetUpdatedAt()),
		Username:  u.GetUsername(),
		Bot:       u.GetBot(),
		TwitchId:  u.GetTwitchID(),
	}
}

func (c *Client) Update(ctx context.Context, user users.User) error {
	ctx, cancel := context.WithCancel(ctx)
	defer cancel()

	_, err := c.UserManagementClient.Update(ctx, &identity.UpdateRequest{User: userToProto(user)})
	if err != nil {
		return err
	}

	return nil
}

func (c *Client) Delete(ctx context.Context, id string) error {
	ctx, cancel := context.WithCancel(ctx)
	defer cancel()

	_, err := c.UserManagementClient.Delete(ctx, &identity.DeleteRequest{})
	return err
}
