package cmd

import (
	"fmt"
	"os"

	"github.com/spf13/cobra"
	"gitlab.com/bancast/streamtools/twitch-tools/log-twitch/cmd/listen"
	"gitlab.com/bancast/streamtools/twitch-tools/pkg/logging"
)

func NewRootCmd() *cobra.Command {
	cmd := &cobra.Command{
		Use:   "log-twitch",
		Short: "Read messages from rabbitmq and output them to stdout",
	}

	logger := logging.SetupLoggerFlags(cmd.PersistentFlags())

	cmd.AddCommand(listen.NewListenCommand(logger))

	return cmd
}

func Execute() {
	rootCmd := NewRootCmd()

	if err := rootCmd.Execute(); err != nil {
		fmt.Fprintln(os.Stderr, err)
		os.Exit(1)
	}
}
