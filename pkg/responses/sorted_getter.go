package responses

import (
	"context"
	"sort"
)

var _ Store = &SortedStore{}

func NewSortedGetter(store Store) *SortedStore {
	return &SortedStore{
		Store: store,
	}
}

type SortedStore struct {
	Store
}

func (g *SortedStore) List(ctx context.Context, twitch_id string) ([]Response, error) {
	responses, err := g.Store.List(ctx, twitch_id)
	if err != nil {
		return nil, err
	}

	// Sort by priority
	sort.SliceStable(responses, func(i, j int) bool {
		return responses[i].GetPriority() < responses[j].GetPriority()
	})

	return responses, nil
}
