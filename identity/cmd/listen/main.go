package listen

import (
	"context"
	"fmt"

	"github.com/spf13/cobra"
	"gitlab.com/bancast/streamtools/twitch-tools/pkg/database"
	dbTokens "gitlab.com/bancast/streamtools/twitch-tools/pkg/database/tokens"
	dbUsers "gitlab.com/bancast/streamtools/twitch-tools/pkg/database/users"
	gateway "gitlab.com/bancast/streamtools/twitch-tools/pkg/grpc"
	"gitlab.com/bancast/streamtools/twitch-tools/pkg/instrumentation"
	"gitlab.com/bancast/streamtools/twitch-tools/pkg/logging"
	"gitlab.com/bancast/streamtools/twitch-tools/pkg/tokens"
	"gitlab.com/bancast/streamtools/twitch-tools/protos/twitch/identity"

	tokensIdentity "gitlab.com/bancast/streamtools/twitch-tools/protos/identity"
	"google.golang.org/grpc"
)

func NewListenCommand(logger *logging.Logger) *cobra.Command {
	cmd := &cobra.Command{
		Use:   "listen",
		Short: "Listen for new messages",
	}

	grpcServerFlags := gateway.SetupServerFlags(cmd.PersistentFlags())
	dbFlags := database.SetupFlags(cmd.PersistentFlags())
	instrumentationFlags := instrumentation.SetupFlags(cmd.PersistentFlags())

	grpcServerFlags.WithOptions(
		gateway.WithAllowedMethods([]string{
			"/UserManagement/Create",
			"/UserManagement/Read",
			"/UserManagement/Lookup",
		}),
	)

	cmd.RunE = func(cmd *cobra.Command, args []string) error {
		logger := logger.Logr()
		ctx, cancel := context.WithCancel(cmd.Context())
		defer cancel()

		go instrumentationFlags.RunMetricsServer(ctx, logger.WithName("metrics"))

		db := dbFlags.MustDatabase(ctx, logger)
		defer db.Close()

		logger.V(3).Info("creating userStore adaptor")
		userStore, err := dbUsers.NewUserStore(ctx, db)
		if err != nil {
			return fmt.Errorf("failed to create userStore adaptor: %w", err)
		}

		logger.V(3).Info("creating tokenStore adaptor")
		tokenStore, err := dbTokens.NewTokenStore(ctx, db)
		if err != nil {
			return fmt.Errorf("failed to create tokenStore adaptor: %w", err)
		}
		tokenProvisioner := grpcServerFlags.TokenProvisioner()
		logger.Info("created provisioner", "provisioner", tokenProvisioner)
		tokenController := tokens.NewController(ctx, tokenStore, tokenProvisioner)

		logger.Info("creating user management server")
		userServer := UserManagementServer{store: userStore, logger: logger.WithName("user-management")}

		logger.Info("starting server", "server", "user-server")
		err = grpcServerFlags.RegisterGateway(
			ctx, logger,
			func(ctx context.Context, grpcServer *grpc.Server) error {
				logger.Info("registering management server")
				identity.RegisterUserManagementServer(grpcServer, userServer)
				return nil
			},
			identity.RegisterUserManagementHandlerFromEndpoint,
		)
		if err != nil {
			return fmt.Errorf("failed to run with gateway: %w", err)
		}

		logger.Info("creating token provisioner server")
		tokenProvisionerServer := TokenProvisionerServer{controller: tokenController, logger: logger.WithName("token-management")}
		logger.Info("starting server", "server", "token-provisioner")
		err = grpcServerFlags.RegisterGateway(
			ctx, logger,
			func(ctx context.Context, grpcServer *grpc.Server) error {
				tokensIdentity.RegisterTokenProvisionerServer(grpcServer, tokenProvisionerServer)
				return nil
			},
			tokensIdentity.RegisterTokenProvisionerHandlerFromEndpoint,
		)
		if err != nil {
			return fmt.Errorf("failed to run with gateway: %w", err)
		}

		logger.Info("creating token management server")
		tokenManagementServer := TokenManagementServer{controller: tokenController, logger: logger.WithName("token-management")}
		logger.Info("starting server", "server", "token-manage")
		err = grpcServerFlags.RegisterGateway(
			ctx, logger,
			func(ctx context.Context, grpcServer *grpc.Server) error {
				tokensIdentity.RegisterTokenManagementServer(grpcServer, tokenManagementServer)
				return nil
			},
			tokensIdentity.RegisterTokenManagementHandlerFromEndpoint,
		)
		if err != nil {
			return fmt.Errorf("failed to run with gateway: %w", err)
		}

		return grpcServerFlags.Listen(ctx, logger.WithName("server"))
	}

	return cmd
}
