package main

import "gitlab.com/bancast/streamtools/twitch-tools/webhookd/cmd"

func main() {
	cmd.Execute()
}
