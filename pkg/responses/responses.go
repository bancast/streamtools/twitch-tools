package responses

import (
	"context"
)

type ContentType string

const (
	// e.g. "Hello watcher!"
	ContentTypeString ContentType = "string"
	// e.g. "Hello %(msg.User.DisplayName)!"
	ContentTypeFormatString = "formatted"
	// e.g. "local parts = std.split(msg.Message, \" \"); \"Hello %s\" % parts[1]"
	ContentTypeJsonnet = "jsonnet"
)

type Response interface {
	GetID() string

	GetPriority() int
	// Regex
	// "!so (.+)"
	GetPattern() string
	GetContentType() ContentType
	/// Jsonnet code
	// "Hello %(msg.User.DisplayName)s, hope you're having a great day!"
	GetContent() string
}

type Store interface {
	List(context.Context, string) ([]Response, error)
	Create(context.Context, string, Response) (string, error)
	Read(context.Context, string) (Response, error)
	Update(context.Context, Response) error
	Delete(context.Context, string) error
}
